#!/usr/bin/env python3
#
# This module contains the TestCasesTable object which represents the main
# test cases table with an internal structure of the form:
#
# { image-release0: [(test-case-name0, exect_type, priority,
#                                   [ deployment-type0, deployment-type1 ... ],
#                                   [ image-type0, image-type1, image-type2 ... ]),
#                    (test-case-name1, exect_type, priority,
#                                   [ deployment-type0, deployment-type1 ... ],
#                                   [ image-type0, image-type1, image-type2 ... ]),
#                    ...],
#   image-release1: [(test-case-name0, exect_type, priority,
#                                   [ deployment-type0, deployment-type1 ... ],
#                                   [ image-type0, image-type1, image-type2 ... ]),
#                    (test-case-name2, exect_type, priority,
#                                   [ deployment-type0, deployment-type1 ... ],
#                                   [ image-type0, image-type1, image-type2 ... ]),
#                    ...],
#  ...
# }
#
# This structure is referenced by the `self._testcases_table` instance variable
# and can be accessed by other modules using the `get` method.
#
# Copyright (C) 2019 Collabora Ltd
# Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import base64
import collections
import os
import copy
import pygit2
import yaml
import logging
import fnmatch
from collections import defaultdict
from subprocess import run, SubprocessError

from config import TestCasesRepositoryConfig, config

test_cases_repo = '/tmp/apertis-test-cases'
test_cases_files_dir = 'test-cases'
atc_renderer_dir = 'atc_renderer'
images_dir = 'images'

priority_int_table = { 'critical': 0, 'high': 1, 'medium': 2, 'low': 3 }


def _get_deployments_supported_by_platform(platform):
    for platform_group in config.instance.images.platform_groups:
        if platform in platform_group.platforms:
            yield from platform_group.deployments


# NOTE: this is also used by the functional test cases!
def get_default_branch(repo):
    # We can't just access the default branch via 'repo.head', because this is
    # a bare clone, so the value there is actually invalid. Instead, make sure
    # we get the *remote*'s head, which is the behavior we'd really want anyway.
    remote_head = repo.references['refs/remotes/origin/HEAD'].target
    return remote_head.split('/')[-1]


class TestCasesTable(object):
    """
    This object represents the table for all the test cases from the
    test case repository.
    """

    def __init__(self):
        self._tmp_table = []
        self._tmp_dict = {}
        self._testcases_table = {}
        self._testcases_data = {}
        self._testcases_images = {}
        self._tmp_tcdata = defaultdict(dict)
        self._tmp_tcimages = defaultdict(dict)

    def initialize(self):
        if os.path.exists(test_cases_repo):
            self._repo = pygit2.Repository(test_cases_repo)
        else:
            self._repo = self._clone()

        self.generate_tc_table_data_image()

    def get(self, image_deployment_and_type_and_release=None):
        # Filter manual test cases using image_type, image_deployment and image_release
        if image_deployment_and_type_and_release:
            imgdeployment, imgtype, imgrelease = image_deployment_and_type_and_release
            # Checking tests for an unsupported deployment returns empty list []
            if imgdeployment not in _get_deployments_supported_by_platform(imgtype):
                return []
            if not imgrelease in self._testcases_table:
                return []
            tmp_lst = [ e for e in self._testcases_table[imgrelease] if \
                        e[1] in ['manual', 'all'] and \
                        imgdeployment in e[3] and imgtype in e[4] ]

            # Sort test cases by priority.
            return sorted(tmp_lst, key=lambda tc: priority_int_table[tc[2]])
        else:
            return self._testcases_table

    def _clone(self):
        def init_remote(repo, name, url):
            remote = repo.remotes.create(name, url, '+refs/*:refs/*')
            repo.config[f'remote.{name}.mirror'] = True
            return remote

        url = config.instance.test_cases_repository.url
        return pygit2.clone_repository(url, test_cases_repo, bare=True,
                                       remote=init_remote)

    def pull(self):
        """Method to pull git repository updates"""
        try:
            self._repo.remotes["origin"].fetch(prune=pygit2.GIT_FETCH_PRUNE)
        except Exception as e:
            logging.error(e)

    def generate_tc_table_data_image(self):
        invalid_platform_patterns = collections.defaultdict(list)

        # List all branches starting with the given branch prefix (e.g.
        # "prefix/") and generates for each the test cases table
        prefix = config.instance.test_cases_repository.branch_prefix.encode('utf-8')
        for ref in self._repo.raw_listall_branches(pygit2.GIT_BRANCH_LOCAL):
            if prefix in ref and ref.startswith(prefix):
                release = ref.decode('utf-8').removeprefix(prefix.decode('utf-8'))
                self._tmp_table = []
                tree = self._repo.revparse_single(ref).tree
                for root_entry in tree:
                    if root_entry.name == test_cases_files_dir:
                        for entry in root_entry:
                            if ( entry.type == pygit2.GIT_OBJ_BLOB and
                                    entry.name.endswith(('.yml', '.yaml')) ):
                                self._make_tc_table_and_data(entry, release,
                                                             invalid_platform_patterns)
                    if root_entry.name == atc_renderer_dir:
                        for entry in root_entry:
                            if entry.name == images_dir:
                                for images in entry:
                                    self._make_tc_images(images, release)
                ref = ref.replace(prefix, b'').decode("utf-8")
                self._tmp_dict[ref] = copy.deepcopy(sorted(self._tmp_table))
        self._testcases_table  = copy.deepcopy(self._tmp_dict)
        self._testcases_data   = copy.deepcopy(self._tmp_tcdata)
        self._testcases_images = copy.deepcopy(self._tmp_tcimages)

        # Wait until the end to log about invalid platforms in order to avoid
        # spamming the logs with the same invalid platform used in different
        # places.
        MAX_WARNINGS_PER_PATTERN = 3
        for platform_pattern, used_by in invalid_platform_patterns.items():
            used_by_string = ' '.join(sorted(used_by[:MAX_WARNINGS_PER_PATTERN],
                                                reverse=True))
            if len(used_by) > MAX_WARNINGS_PER_PATTERN:
                used_by_string += '...'

            logging.warning(f"Invalid platform '{platform_pattern}' used by"
                            f' {len(used_by)} test case(s): {used_by_string}')

    def get_index_files_data(self):
        return self._testcases_data

    def get_tc_data(self, release, tc_file):
        if self.get_index_files_data().get(release):
            return self.get_index_files_data().get(release).get(tc_file)
        return None

    def get_tc_images(self, release):
        return self._testcases_images.get(release)

    def get_default_branch(self):
        return get_default_branch(self._repo)

    def update(self):
        self._tmp_dict = {}
        self.pull()
        self.generate_tc_table_data_image()

    def _make_tc_images(self, tc_blob, release):
        if tc_blob.name.endswith(('.png', '.jpg', '.jpeg', '.gif', '.ogv')):
            blob = self._repo[tc_blob.id]
            if blob.is_binary:
                self._tmp_tcimages[release][tc_blob.name] = base64.b64encode(blob.data).decode('utf-8')

    def _make_tc_table_and_data(self, tc_blob, release, invalid_platform_patterns):
        try:
            tc_data = yaml.safe_load(tc_blob.data)
        except yaml.scanner.ScannerError as e:
            print("yaml format error:", e)
            exit(1)

        tc_name = tc_data['metadata']['name']
        image_type_platforms = []
        for image_type , archs in tc_data['metadata']['image-types'].items():
            for arch in archs:
                # Arch values can be suffixed with the platform's visibility
                # (e.g. amd64-internal).
                if '-' in arch:
                    arch, visibility = arch.split('-', 1)
                    if visibility != 'internal':
                        logging.warning(f"Test case '{tc_name}' uses invalid"
                                        f" visibility '{visibility}'")
                        visibility = 'public'
                else:
                    visibility = 'public'

                matching_platforms = []
                platform_pattern = f'{image_type}-{arch}-*-{visibility}'

                for platform_group in config.instance.images.platform_groups:
                    if platform_group.matches(release=release, version=None):
                        matching_platforms.extend(fnmatch.filter(
                            platform_group.platforms, platform_pattern))

                if matching_platforms:
                    image_type_platforms.extend(matching_platforms)
                else:
                    invalid_platform_patterns[platform_pattern].append(f'{release}/{tc_name}')

        image_deployments = [ e.lower() for e in
                              tc_data['metadata']['image-deployment'] ]
        self._tmp_table.append((tc_name, tc_data['metadata']['exec-type'],
                                tc_data['metadata']['priority'],
                                image_deployments,
                                image_type_platforms))

        blob = self._repo[tc_blob.id]
        self._tmp_tcdata[release][tc_blob.name] = blob.data
