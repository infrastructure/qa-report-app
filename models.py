###################################################################################
# QA Report Application
#
# Database connection and models.
#
# Copyright (C) 2019 Collabora Ltd
# Luis Araujo <luis.araujo@collabora.co.uk>
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from datetime import datetime
import enum
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func, Enum

from config import config

from packaging import version

db = SQLAlchemy()

# return a nested datastructure for each build (for instance, v2020/20201225.0)
# aggregating results by deployment type, like:
# {
#   image_release: v2021
#   image_build: 20221110.1454
#   tag: v2021.7
#   tag_type: release
#   results:
#     "apt": {"executed": 336,
#             "manual": {"fail": 14, "pass": 88, "skip": 0, "total": 102, "incomplete": 0},
#             "automated": {"fail": 15, "pass": 217, "skip": 0, "total": 234, "incomplete": 2}},
#     "lxc": {"executed": 36,
#             "manual": {"fail": 0, "pass": 0, "skip": 0, "total": 0, "incomplete": 0},
#             "automated": {"fail": 6, "pass": 30, "skip": 0, "total": 36, "incomplete": 0}},
#     "nfs": {"executed": 3,
#             "manual": {"fail": 0, "pass": 0, "skip": 0, "total": 0, "incomplete": 0},
#             "automated": {"fail": 0, "pass": 3, "skip": 0, "total": 3, "incomplete": 0}},
#     "ostree": {"executed": 199,
#                "manual": {"fail": 4, "pass": 39, "skip": 0, "total": 43, "incomplete": 0},
#                "automated": {"fail": 3, "pass": 148, "skip": 0, "total": 156, "incomplete": 5}}
# },
# ...
OVERVIEW_SQL = """
WITH
jobs_and_testcases AS (
  SELECT DISTINCT ON (image_release, image_build, image_deployment, image_type, image_architecture, image_platform, testcases.suite)
         jobs.image_release, jobs.image_build, jobs.image_deployment, jobs.image_type, jobs.image_architecture, jobs.image_platform,
         jobs.exec_type, testcases.result, starred.tag, starred.tag_type
    FROM jobs
    LEFT OUTER JOIN starred ON (jobs.image_release = starred.image_release AND jobs.image_build = starred.image_version)
    INNER JOIN testcases ON (jobs.id = testcases.job_id)
   WHERE testcases.suite IS NOT NULL
     AND testcases.suite != 'misc/add-repo.yaml'
     AND testcases.result != 'not_tested'
     AND (jobs.creation_time >= CURRENT_DATE - interval '2 week' OR starred.tag IS NOT NULL)
   ORDER BY image_release, image_build, image_deployment, image_type, image_architecture, image_platform, testcases.suite, lava_job_id DESC
),
grouped_testcases AS (
  SELECT image_release, image_build, image_deployment, image_type, image_architecture, image_platform, tag, tag_type,
         COUNT(*) AS executed,
         COUNT(*) FILTER (WHERE exec_type = 'automated') AS automated,
         COUNT(*) FILTER (WHERE exec_type = 'automated' AND result = 'pass') AS automated_pass,
         COUNT(*) FILTER (WHERE exec_type = 'automated' AND result = 'fail') AS automated_fail,
         COUNT(*) FILTER (WHERE exec_type = 'automated' AND result = 'incomplete') AS automated_incomplete,
         COUNT(*) FILTER (WHERE exec_type = 'automated' AND result = 'skip') AS automated_skip,
         COUNT(*) FILTER (WHERE exec_type = 'manual') AS manual,
         COUNT(*) FILTER (WHERE exec_type = 'manual' AND result = 'pass') AS manual_pass,
         COUNT(*) FILTER (WHERE exec_type = 'manual' AND result = 'fail') AS manual_fail,
         COUNT(*) FILTER (WHERE exec_type = 'manual' AND result = 'incomplete') AS manual_incomplete,
         COUNT(*) FILTER (WHERE exec_type = 'manual' AND result = 'skip') AS manual_skip
    FROM jobs_and_testcases
    GROUP BY image_release, image_build, image_deployment, image_type, image_architecture, image_platform, tag, tag_type
),
grouped_deployments AS (
  SELECT image_release, image_build, image_deployment, tag, tag_type,
         sum(executed) AS executed, sum(automated) AS automated, sum(manual) AS manual,
         sum(automated_pass) AS automated_pass, sum(automated_fail) AS automated_fail, sum(automated_incomplete) AS automated_incomplete, sum(automated_skip) AS automated_skip,
         sum(manual_pass) AS manual_pass, sum(manual_fail) AS manual_fail, sum(manual_incomplete) AS manual_incomplete, sum(manual_skip) AS manual_skip
    FROM grouped_testcases
    GROUP BY image_release, image_build, image_deployment, tag, tag_type
)
SELECT image_release, image_build, tag, tag_type,
       JSONB_OBJECT_AGG(image_deployment,  JSONB_BUILD_OBJECT(
          'executed', executed,
          'automated', (JSONB_BUILD_OBJECT(
                'total', automated,
                'pass', automated_pass,
                'fail', automated_fail,
                'incomplete', automated_incomplete,
                'skip', automated_skip)),
          'manual', (JSONB_BUILD_OBJECT(
                'total', manual,
                'pass', manual_pass,
                'fail', manual_fail,
                'incomplete', manual_incomplete,
                'skip', manual_skip))
       )) AS results
  FROM grouped_deployments
 GROUP BY image_release, image_build, tag, tag_type
 ORDER BY image_build DESC, tag DESC, image_release DESC
"""

def reports_overview(show_all=False):
    entries = db.session.execute(OVERVIEW_SQL).fetchall()

    if config.instance.display is not None and not show_all:
        min_version = version.parse(config.instance.display.min_version)

        return list(filter(
            lambda e: version.parse(e['image_release']) >= min_version,
            entries))

    return list(entries)


class Job(db.Model):
    """Tests jobs."""
    __tablename__ = 'jobs'

    id = db.Column(db.Integer, primary_key = True)
    creation_time = db.Column(db.DateTime(timezone=True), nullable=False,
                              server_default=func.now())
    testcases = db.relationship('TestCase', backref='jobs', lazy=True)

    # Following are LAVA specific fields only for automated tests.
    lava_job_id = db.Column(db.Integer)
    lava_submit_time = db.Column(db.DateTime(timezone=True))
    lava_start_time = db.Column(db.DateTime(timezone=True))
    lava_end_time = db.Column(db.DateTime(timezone=True))
    lava_health = db.Column(db.String(80))
    lava_state = db.Column(db.String(80))
    lava_status = db.Column(db.String(80))
    lava_device_id = db.Column(db.String)
    lava_actual_device_id = db.Column(db.String)

    image_url = db.Column(db.String, nullable=False)
    image_release = db.Column(db.String, nullable=False)
    image_build = db.Column(db.String, nullable=False)
    image_deployment = db.Column(db.String, nullable=False)
    image_type = db.Column(db.String, nullable=False)
    image_architecture = db.Column(db.String, nullable=False)
    image_platform = db.Column(db.String)

    exec_type = db.Column(db.String(80), nullable=False)
    description = db.Column(db.String)
    visibility = db.Column(db.String(20), nullable=False)

    # Tester (lava for automated and tester login name for manual tests)
    tester = db.Column(db.String(50))

    def infer_image_platform(self):
        # manual test results originally did not have image_platform set,
        # so let's add some heuristics to infer it
        # once the NOT NULL constraint can be applied to image_platform
        # this can be removed
        image_platform = self.image_platform
        if not image_platform:
            if 'arm' in self.image_architecture:
                image_platform = 'uboot'
            elif 'sdk' in self.image_type:
                image_platform = 'sdk'
            elif self.image_architecture == 'amd64':
                image_platform = 'uefi'
            else:
                raise Exception(f"Unknown image_platform for job {self.id}: {self.image_type}-{self.image_architecture}-{self.visibility}-{self.image_build}-{self.exec_type}")
        return image_platform

class TestCase(db.Model):
    """Test cases for tests jobs."""
    __tablename__ = 'testcases'

    id = db.Column(db.Integer, primary_key = True)
    creation_time = db.Column(db.DateTime(timezone=True), nullable=False,
                              server_default=func.now())
    results = db.relationship('Result', backref='testcases', lazy=True)

    job_id = db.Column(db.Integer, db.ForeignKey('jobs.id'), nullable=False)
    job = db.relationship('Job', lazy=True)
    suite = db.Column(db.String, nullable=False)
    repository = db.Column(db.String, nullable=False)
    path = db.Column(db.String)
    commit_id = db.Column(db.String)
    result = db.Column(db.String, nullable=False)
    lava_result_id = db.Column(db.Integer)
    lava_url = db.Column(db.String)

    # Notes box.
    notes = db.Column(db.String)


class Result(db.Model):
    """Results for test cases."""
    __tablename__ = 'results'

    id = db.Column(db.Integer, primary_key=True)
    creation_time = db.Column(db.DateTime(timezone=True), nullable=False,
                              server_default=func.now())

    testcases_id = db.Column(db.Integer, db.ForeignKey('testcases.id'),
                             nullable=False)
    name = db.Column(db.String, nullable=False)
    result = db.Column(db.String)
    lava_result_id = db.Column(db.Integer)
    lava_url = db.Column(db.String)


class TagType(enum.Enum):
    release = 1
    weekly = 2

    def __str__(self):
        return f'{self.name}'


class Starred(db.Model):
    """Starred releases."""
    __tablename__ = 'starred'

    id = db.Column(db.Integer, primary_key=True)
    creation_time = db.Column(db.DateTime(timezone=True), nullable=False,
                              server_default=func.now())

    image_release = db.Column(db.String, nullable=False)
    image_version = db.Column(db.String, nullable=False)
    tag = db.Column(db.String(80))
    tag_type = db.Column(Enum(TagType), nullable=False, default=TagType.release)
