###################################################################################
# Module to save test job into the database.
#
# Copyright (C) 2019 Collabora Ltd
# Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import logging
from datetime import datetime

from models import Job, TestCase, Starred


def get_timeobj(time_str):
    if time_str == "None":
        logging.error(f"Invalid date value '%s', workaround https://git.lavasoftware.org/lava/lava/-/issues/493", time_str)
        return None
    # This line fixes the timezone offset converting +00:00 to +0000 so it can
    # correctly be interpreted by Python versions < 3.7
    time_str_fixed_tz = time_str[:-3] + time_str[-2:]
    return datetime.strptime(time_str_fixed_tz, '%Y-%m-%d %H:%M:%S.%f%z')

def save_job(job, db):
    # Visibility for private jobs is of the form: {'group': ['Apertis']} ,
    # so make that just `internal`.
    visibility = job.metadata['visibility']
    if isinstance(visibility, dict):
        visibility = 'internal'

    # Create a job from the database model.
    job_db = Job(lava_job_id           = job.id,
                 lava_submit_time      = get_timeobj(job.data['submit_time']),
                 lava_start_time       = get_timeobj(job.data['start_time']),
                 lava_end_time         = get_timeobj(job.data['end_time']),
                 lava_health           = job.data['health_string'],
                 lava_state            = job.data['state_string'],
                 lava_status           = job.data['status_string'],
                 lava_device_id        = job.metadata['device_type'],
                 lava_actual_device_id = job.data['actual_device_id'],
                 image_url             = job.metadata['image.url'],
                 image_release         = job.metadata['image.release'],
                 image_build           = job.metadata['image.version'],
                 image_deployment      = job.metadata['image.deployment'],
                 image_type            = job.metadata['image.type'],
                 image_architecture    = job.metadata['image.arch'],
                 image_platform        = job.metadata['image.board'],
                 # Default to automated since manual tests are not yet supported.
                 exec_type             = 'automated',
                 description           = job.description,
                 visibility            = visibility)

    # Iterate over each test case of the job and add it into the testcases table.
    for suite, tests in job.results.items():
        for test in tests:
            job_db.testcases.append(TestCase(suite          = suite,
                                             repository     = test.repository,
                                             path           = test.path,
                                             commit_id      = test.commit_id,
                                             result         = test.result,
                                             lava_result_id = test.id,
                                             lava_url       = test.url))

    logging.info('Saving into database job id: %d', job.id)
    db.session.add(job_db)
    db.session.commit()

def save_manual_job(username, manual_tests_results, db,
                    img_release, img_version,
                    image_type, image_deployment):

    img_type, img_arch, img_platform, visibility = image_type.split('-')
    # Assume at the beginning this is updating a record.
    update = True

    # Always make sure there is no space for these fields.
    image_release, image_version = img_release.strip(), img_version.strip()

    # Search for an existing manual job record for this image type, and use that
    # for updating record.
    job_db = Job.query.filter_by(image_release      = image_release,
                                 image_build        = image_version,
                                 image_deployment   = image_deployment,
                                 image_type         = img_type,
                                 image_architecture = img_arch,
                                 image_platform     = img_platform,
                                 visibility         = visibility,
                                 exec_type          = "manual").first()
    # Otherwise create a new entry in the DB for this image-type.
    # NOTE: 'image_url' is empty, this can be improved later.
    if not job_db:
        job_db = Job(image_release      = image_release,
                     image_build        = image_version,
                     image_type         = img_type,
                     image_architecture = img_arch,
                     image_platform     = img_platform,
                     image_deployment   = image_deployment,
                     visibility         = visibility,
                     exec_type          = "manual",
                     image_url          = "")
        # It's a new job entry, not a record update in DB.
        update = False

    for elem, value in manual_tests_results.items():
        # Every 'test_case: result' element has a '__test_case_notes: notes'
        # metadata entry, skip it and fetch that directly from the dictionary.
        if elem.startswith('__') and elem.endswith('_notes'):
            continue

        test_case = elem
        result = value
        # Fetch Notes for this test case.
        notes = manual_tests_results.get('__'+test_case+'_notes', '')

        updated_testcase = False

        if update:
            # Find existing test case record and update value.
            for testcase_record in job_db.testcases:
                if testcase_record.suite == test_case:
                    testcase_record.result = result
                    testcase_record.notes = notes

                    updated_testcase = True
                    break

        if not updated_testcase:
            # NOTE: 'repository' is empty, this can be improved later.
            job_db.testcases.append(TestCase(suite = test_case, result = result,
                                             notes = notes, repository = ""))
    # Always update Tester name.
    job_db.tester = username

    if not update:
        logging.info('Saving manual job for image type: %s,release: %s,' \
                     'version: %s', image_type, image_release, image_version)
        db.session.add(job_db)
    else:
        logging.info('Updating manual job record for image type: %s,release: %s,' \
                     'version: %s', image_type, image_release, image_version)
    db.session.commit()


def update_tags(image_release, image_version, tag, is_checked, tag_type, db):
    starred = Starred.query. \
              filter_by(image_release = image_release,
                        image_version = image_version).one_or_none()

    if is_checked == 'true':
        stag = tag.strip()
        if starred:
            # Only make sure to update tag if job already starred.
            logging.info("Updating tag for report %s/%s to: '%s' (%s)",
                         image_release, image_version, stag, tag_type)
            starred.tag = stag
            starred.tag_type = tag_type
        else:
            # Otherwise create a new entry for the job in Starred table.
            logging.info("Add release report %s/%s with tag: '%s' (%s)",
                         image_release, image_version, stag, tag_type)
            starred = Starred(image_release = image_release,
                              image_version = image_version,
                              tag = stag,
                              tag_type = tag_type)
            # Add starred job.
            db.session.add(starred)
    elif starred is not None:
        # Request to untag, then remove it.
        logging.info('Remove release report %s/%s',
                     image_release, image_version)
        db.session.delete(starred)

    db.session.commit()
