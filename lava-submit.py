#!/usr/bin/python3
###################################################################################
# Apertis LAVA QA tool wrapper
# Copyright (C) 2018 Collabora Ltd
# Andrej Shadura <andrew.shadura@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import argparse
import json
from urllib.error import URLError
from urllib.request import urlopen
import xmlrpc.client as xmlrpclib

import lqa_tool.utils
lqa_tool.utils.print_add_msg = lambda a,b: None

from lqa_api.waitqueue import WAIT_DEFAULT_TIMEOUT
from lqa_api.exit_codes import APPLICATION_ERROR
from lqa_tool.commands.submit import SubmitCmd as BaseSubmitCmd, Profiles
from lqa_tool.exceptions import ProfileNotFound
from lqa_tool.settings import settings, lqa_logger
from lqa_tool.utils import merge_profiles

class SubmitCmd(BaseSubmitCmd):

    def run(self):
        """Submit job id
        """

        # set wait_timeout to make _run put job ids into the queue
        self.args.wait_timeout = True

        variables = {}
        if self.args.template_vars:
            for item in self.args.template_vars:
                k, v = item.split(':', 1)
                variables[k] = v

        if not 'image_date' in variables and not 'image_name' in variables:
            lqa_logger.error('image_date and image_name are required template variables')
            exit(APPLICATION_ERROR)

        if self.args.profile:
            # A profile file is required for using profiles
            if not self.args.profile_file:
                raise ProfileNotFound

            # Get the profiles from the <profiles>.yaml file
            self.profiles = Profiles(self, self.args.profile_file)
            # Fetch the main profile and merge later with any sub profile
            main_profile = self.profiles.config.get('main-profile', {})

            # Filter profiles from the profiles file matching -p.
            profiles = [
                profile
                for profile in self.profiles.config.get('profiles', [])
                if profile['name'] == self.args.profile]
            # Exit with failure if no profile in the filtered list.
            if not profiles:
                lqa_logger.error(
                    "error: profile {} not found in profile file {}"
                    .format(self.args.profile, self.args.profile_file))
                exit(APPLICATION_ERROR)
            # Run with found profiles. 
            # There can exist profiles with the same name.
            for profile in profiles:
                self._run(merge_profiles(main_profile, profile))
        else:
            lqa_logger.error("profile name required")
            exit(APPLICATION_ERROR)

        submitted = []
        if self.waitq.has_jobs():
            osname, imagetype, arch, board = self.args.profile.split('-', maxsplit=3)
            flavour, _, board = board.rpartition('-')
            metadata = {
                'image.version': variables['image_date'],
                'image.release': variables['release'],
                'image.type': imagetype,
                'image.arch': arch,
                'image.board': board,
                'image.osname': osname,
                'image.flavour': flavour,
                'image.name': variables['image_name']
            }
            for job in self.waitq:
                submitted.append(job.id)
                request = {
                    'status_string': 'submitted',
                    'metadata': metadata,
                    'id': job.id,
                    'token': self.args.callback_secret
                }
                try:
                    urlopen(self.args.callback_url, data=json.dumps(request).encode('UTF-8'))
                except URLError:
                    pass
        if self.args.jobid_file:
            json.dump({ "jobids": submitted }, self.args.jobid_file)


parser = argparse.ArgumentParser(description="submit LAVA jobs for Apertis")
parser.add_argument('-c', '--config', metavar='CONFIG.yaml',
                    help="set configuration file")
parser.add_argument('--log-file', type=str, help="set the log file")
parser.add_argument('--jobid-file', type=argparse.FileType("w"), help="output file for the list of created job ids")
parser.add_argument('submit_job', nargs='*', type=str,
                    metavar='JOB_FILE.yaml',
                    help="job file to submit")
parser.add_argument('-g', '--profile-file', metavar='PROFILE.yaml',
                    help="set profile file", required=True)
parser.add_argument('-n', '--dry-run', action='store_true',
                    help="Dry-run, do everyting apart from "
                    "submitting")
parser.add_argument('-p', '--profile', type=str, required=True,
                    help="specify the profiles to use")
parser.add_argument('--callback-secret', type=str, required=True,
                    help="specify the secret token to use for the callback")
parser.add_argument('-t', '--template-vars', action='append',
                    type=str, help="set 'field:value' "
                    "template variables/values "
                    "(can be given multiple times")
parser.add_argument('-u', '--callback-url', type=str, required=True,
                    help="specify the callback URL")
parser.add_argument('-v', '--verbose', action='store_true',
                    help="Verbose mode (e.g. print the resulting "
                    "yaml)")
parser.add_argument('--wait',  nargs='?', type=str,
                    metavar='TIMEOUT', dest='wait_timeout',
                    const=WAIT_DEFAULT_TIMEOUT,
                    help=argparse.SUPPRESS)
parser.add_argument('--debug-vars', action='store_true',
                    help=argparse.SUPPRESS)
parser.add_argument('--priority',
                    choices=['high', 'medium', 'low'],
                    help="Set the job priority"),
parser.add_argument('--live', action='store_true',
                    help=argparse.SUPPRESS)
parser.add_argument('--check-image-url', action='store_true',
                    help=argparse.SUPPRESS)
args = parser.parse_args()
try:
    settings.load_config(config_file=args.config, log_file=args.log_file)
    SubmitCmd(args).run()
except ProfileNotFound:
    parser.error('Please specify a profile file using the -g option')
except xmlrpclib.ProtocolError as e:
    # Catch any XMLRPC protocol error at this point.
    lqa_logger.error("xmlrpc error: {}".format(e))
    exit(APPLICATION_ERROR)
except KeyboardInterrupt:
    pass
