ARG DEBIAN_FRONTEND=noninteractive

FROM debian:bullseye AS unittest

LABEL description="Apertis QA test results collection and reporting application - unittest environment"

RUN apt update && \
    apt install --no-install-recommends -y \
        ca-certificates \
        git \
        python3-all \
        python3-apscheduler \
        python3-blinker \
        python3-gitlab \
        python3-phabricator \
        python3-pip \
        python3-psycopg2 \
        python3-setuptools \
        python3-urlobject \
        python3-wheel \
        python3-yaml \
        python3-yattag \
        wait-for-it

# See https://phabricator.apertis.org/T9563 and https://github.com/pallets-eco/flask-sqlalchemy/issues/1122 for context on pinning SQLAlchemy's version
RUN pip3 install \
    authlib \
    Flask==2.3 \
    Werkzeug==2.3 \
    Flask-Dance \
    Flask-migrate \
    Flask-SQLAlchemy \
    SQLAlchemy==1.4.46 \
    Flask-Testing \
    flask-healthz \
    flask_wtf \
    hupper \
    lxml \
    needle \
    pydantic==1.10.2 \
    pygit2==1.7.1 \
    waitress

RUN apt update && \
    apt install --no-install-recommends -y \
        chromium-driver \
        perceptualdiff \
        python3-pytest \
        python3-testing.postgresql

# this has to come after postgres is installed, as it breaks the `adduser postgres` call
RUN apt update && \
    apt install --no-install-recommends -y \
        libnss-unknown # needed to run as unprivileged user, as required by postgres instantiated by testing.postgresql

RUN git clone https://gitlab.apertis.org/tests/apertis-test-cases.git \
              --mirror \
              /var/lib/apertis-test-cases

FROM debian:bullseye AS runtime

RUN useradd --uid 1001 -U -d /app qa-report-app

RUN apt update && \
    apt install --no-install-recommends -y \
        ca-certificates \
        git \
        python3-all \
        python3-apscheduler \
        python3-blinker \
        python3-gitlab \
        python3-packaging \
        python3-phabricator \
        python3-pip \
        python3-psycopg2 \
        python3-setuptools \
        python3-urlobject \
        python3-wheel \
        python3-yaml \
        python3-yattag \
        libnss-unknown \
        wait-for-it && \
   	rm -r /var/lib/apt/lists /var/cache/apt/archives

# See https://phabricator.apertis.org/T9563 and https://github.com/pallets-eco/flask-sqlalchemy/issues/1122 for context on pinning SQLAlchemy's version
RUN pip3 install --no-cache-dir authlib Flask Flask-Dance Flask-migrate Flask-SQLAlchemy SQLAlchemy==1.4.46 flask-healthz flask_wtf hupper pydantic==1.10.2 pygit2==1.7.1 waitress

LABEL description="Apertis QA test results collection and reporting application - runtime environment"

WORKDIR /app

ADD . /app

# Use an unprivileged port by default
EXPOSE 28080

# Use an unprivileged user
USER qa-report-app

CMD ["/app/run-qa-report"]
