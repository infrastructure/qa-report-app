#!/usr/bin/env python3
###################################################################################
# Parser for the test case renderer.
# This module defines the test case format and implement a parser for the format.
#
# Copyright (C) 2018
# Luis Araujo <luis.araujo@collabora.co.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import logging
import sys
import yaml

from atc_renderer.exceptions import ParserTypeError, ParserMissingFieldError
from config import config

tests_types = ['compatibility', 'functional', 'performance', 'sanity', 'system']
priorities = ['critical', 'high', 'low', 'medium']
execution_type = ['all', 'automated', 'manual']

# This structure defines the test case YAML file format.
test_case_format = {
    'metadata': (True, {
        'name':              (True,  ""),
        'format':            (False, ""),
        'image-types':       (True,  []),
        'image-deployment':  (True,  []),
        'type':              (True,  tests_types),
        'exec-type':         (True,  execution_type),
        'priority':          (True,  priorities),
        'maintainer':        (False, ""),
        'description':       (True,  ""),
        'resources':         (False, []),
        'pre-conditions':    (False, []),
        'expected':          (True,  []),
        'notes':             (False, []),
        'post-conditions':   (False, [])
    }),
    'install': (False, {
        'git-repos' : (True, []),
        'deps': (False, [])
    }),
    'run': (True, {
        'steps': (True, [])
    }),
    'parse': (False, {})
}


def _parse_format(test_case, test_case_format):
    for tagf, valuestr in test_case_format.items():
        mandatory, valuef = valuestr
        value = test_case.get(tagf)
        if not value:
            if mandatory:
                logging.warning("Mandatory field missing: %s", tagf)
            # Test case doesn't have this non-mandatory tag, so just continue.
            continue

        if tagf == 'image-types':
            if not isinstance(value, dict):
                logging.warning("Incorrect type for field: %s", tagf)
                continue

            for img_type, img_archs in value.items():
                supported_variants = ['any', *config.instance.images.variants]
                supported_archs = ['any', *config.instance.images.architectures]

                # Note that 'type' is, in this case, used for the same meaning
                # as 'variant'.
                if img_type not in supported_variants:
                    logging.warning("value '%s' not found in '%s' for field '%s'", img_type, supported_variants, tagf)

                for e in img_archs:
                    if e not in supported_archs:
                        logging.warning("value '%s' not found in '%s' for field '%s'", e, supported_archs, tagf)
            continue

        if type(value) == str and type(valuef) == list:
            if tagf in ['type', 'exec-type', 'priority']:
                try:
                    valuef.index(value)
                except ValueError:
                    logging.warning("value '%s' not found in '%s' for field '%s'", value, valuef, tagf)
                continue

        if type(value) != type(valuef):
            logging.warning("Incorrect type for field: %s", tagf)
            continue

        if type(value) == dict:
            _parse_format(value, valuef)

    return True

def parse_format(test_case):
    return _parse_format(test_case, test_case_format)


if '__main__' == __name__:
    testcase = sys.argv[1]
    with open(testcase) as test_case:
        test_case_data = yaml.safe_load(test_case)

    parse_format(test_case_data)
