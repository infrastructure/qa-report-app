###################################################################################
# LAVA CI callback webservice
#
# Test analyser module
#
# Copyright (C) 2018 Collabora Ltd
# Andrej Shadura <andrew.shadura@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from itertools import chain, groupby

def failed_testcases(results):
    # ignore successful, skipped test cases and test cases ending with _sh
    # test cases ending with _sh are automatically generated by
    # run-test-in-systemd and don't carry much useful information
    return filter(lambda t: t.result != 'pass' and t.result != 'skip' and not t.name.endswith('_sh') and t.name != 'run-test', results)

def find_failures(jobs):
    """
    Find failed tests, group them by the testcase definition

    [<TestJob>, <TestJob>...] -> [('testdef_1', <testcases...>),
                                  ('testdef_2', <testcases...>)]

    <testcases...> elements are iterators
    """
    testdefinition_key = lambda t: t.testdefinition
    completed_jobs = filter(lambda j: j.status == 'complete' and not j.is_wip, jobs)
    testcases = sorted(
        chain.from_iterable(
            # join from all jobs
            [
                # join all results
                chain.from_iterable(j.results.values()) for j in completed_jobs
            ]
        ),
        key=testdefinition_key
    )
    return groupby(failed_testcases(testcases), testdefinition_key)
