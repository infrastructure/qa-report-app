#!/usr/bin/env python3

import argparse
import os

from config import config


def main():
    parser = argparse.ArgumentParser(description='Wait for the database to start')
    parser.add_argument('-c', '--config', metavar='CONFIG',
                        help="configuration file")
    args = parser.parse_args()

    config.skip_bug_tracking_validation = True

    if args.config:
        config.load_config_from_file(args.config)
    else:
        config.load_config_from_env_only()

    os.execlp('wait-for-it', 'wait-for-it', '--timeout=10',
              f'{config.instance.db.host}:{config.instance.db.port}')


if __name__ == '__main__':
    main()
