# QA Test Report APP

## Configuration

Documentation for configuring this application is available on [the Apertis
website](https://www.apertis.org/guides/qa-report-app-admin/).

## Test Cases Table

This table contains the mapping between test cases and their supported image type
platforms in the form of a python structure:

```
{ image-release0: [(test-case-name0, exect_type, priority,
                                  [ deployment-type0, deployment-type1 ... ],
                                  [ image-type0, image-type1, image-type2 ... ]),
                   (test-case-name1, exect_type, priority,
                                  [ deployment-type0, deployment-type1 ... ],
                                  [ image-type0, image-type1, image-type2 ... ]),
                   ...],
  image-release1: [(test-case-name0, exect_type, priority,
                                  [ deployment-type0, deployment-type1 ... ],
                                  [ image-type0, image-type1, image-type2 ... ]),
                   (test-case-name2, exect_type, priority,
                                  [ deployment-type0, deployment-type1 ... ],
                                  [ image-type0, image-type1, image-type2 ... ]),
                   ...],
  ...
}
```

This table basically tells to the qa report application the available test cases
to report and in which platforms and image deployments they are enabled with their
respective priority.

The `apertis-test-cases` local repository and the table are automatically
updated by `apertis-qa-report` to follow changes in the tests (for example a
test case or image type is added or removed).

## Metadata Requirement

The test cases files and LAVA job test definitions are closely related to the
QA Report Application, therefore, in order for the application to properly report
test cases results, a certain metadata consistency must exist between these
components.

The following elements must have the same name:

- The test case file name.
- The test case `metadata: name` field.
- The test case name in the LAVA job definition file.

Otherwise mismatch between names might occur and results won't be reported.

## Testing

### Development test

It is possible to test the `qa-report-app` locally during development, without GitLab authentication or Phabricator connection, by running:
```
$ docker-compose up
```

This allows to connect to the `qa-report-app` server using http://127.0.0.1:28080

While in development mode, any change to the source file are automatically retrieved by the server and the app is restarted allowing to check development directly.

After stopping the `qa-report-app` server using `Ctrl-c`, it is possible to remove test container by running:
```
$ docker container rm qa-report-app-apertis-qa-report-1
$ docker image rm registry.gitlab.apertis.org/infrastructure/qa-report-app/runtime
```

#### Cloning the production database

In case a database populated with real data is required, it can be exported from
production and imported locally. To do it:

- Retrieve the postgresql password at `apertis/qa-report-app/postgresql` in our
  [Hashicorp Vault instance](https://vault.collabora.com:8200/). (Contact the
  sysadmin team if you don't have access.)
- Ensure you have [set up kubectl to connect to our DigitalOcean instance
  instance](https://docs.digitalocean.com/products/kubernetes/how-to/connect-to-cluster/).
- Dump the database contents to the local system:
  ```
  $ kubectl -n qa-report-app exec -i postgresql-0 -- \
    sh -c "PGUSER=qa-report-app PGPASSWORD='$PGPASSWORD' pg_dump qa-report-app" \
    > database/postgresql/data/test_results_dump.sql
  ```
  where `$PGPASSWORD` is the postgresql password retrieved previously.
- Drop the existing database and recreate one from scratch (make sure to stop any container
  using the database - e.g. the qa-report-app container itself):
  ```
  $ docker exec -t qa-report-app_postgres_1 \
    sh -c "dropdb -U test_results test_results; createdb -U test_results test_results"
  ```
- Import the database into the local Docker container:
  ```
  $ docker exec -t qa-report-app_postgres-1 \
    sh -c "psql test_results test_results < /var/lib/postgresql/data/test_results_dump.sql"
  ```

### Bug tracking support

In order to test bug tracking support a Gitlab project with issues should be used and the following configurations applied:

   ```
   bug-tracking: gitlab
   gitlab-project: YOUR_GITLAB_PROJECT
   gitlab-token: YOUR_TOKEN
   ```

Additionally, tweak `docker-compose.override.yml` removing `--skip-bug-tracking`

After that using curl is possible to submit test results as LAVA should do by executing:

  ```
  $ curl --header "Content-Type: application/json" --request POST \
         --data-binary "@tests/test_data/job_data_1377334" http://localhost:28080
  ```

### Notifications

Test as follow:

1) Configure `config.yaml` inside the `secrets` directory.

2) Build docker image:

```
$ docker build -t qa-report:test .
```

3) Run docker image:

```
$ docker run --name qa-report --rm qa-report:test
```

4) From another terminal, execute the test script to register a sample job:

```
$ cd tests/
$ ./test_register_job.py <APP_HOST>
```

The <APP_HOST> for the qa-report image can be found with the command:

```
$ docker inspect qa-report | grep IPAddress
```

5) Send job data:

```
$ curl -X POST -d @test_data/job_data_1491564 http://<APP_HOST>:28080/
```

This should show the following message:

`Marked job 1491564 as finished with status complete`

Now the Phabricator notification should have been sent, and depending on the
config.yaml file values, the mattermost and email notifications too.

### Unit tests

The Dockerfile ships a `unittest` step for generating images with all the test
dependencies, including a full local postgres instance for functional testing.

Launch `pytest` from the project root or from the `tests/` directory to run
the automated tests:

```
$ docker build -t qa-report-app-unittest --target unittest .
$ docker run --rm -t --user $(id -u):$(id -g) --security-opt label=disable -v $(pwd):/app -w /app qa-report-app-unittest pytest-3
```

### Visual regression testing

The testsuite also run visual regression tests that take screenshots of the
application and compare them to known-good images.

When updating the visual appearance of the application, run the testsuite while
exporting the `NEEDLE_SAVE_BASELINE=1` environment variable: this will force the
known-good images to be regenerated based on the current screenshots.
Review the changes to ensure they match your expectations and that there are no
unwanted changes, and commit the results.

### TestJob object

The `testobjects.py` can be executed standalone passing a job_data file to check
the final JSON format of the LAVA test job object.

```
$ python3 testobjects.py tests/test_data/job_data_1491564
```

## Database configuration

The application requires a connection to a PostgreSQL DB which can be configured
with environment variables. Check the `docker-compose.yaml` file to see how it
can be done.

## Deployment

This application is deployed as part of our overall Kubernetes infrastructure
setup; please consult the [general deployment
documentation](https://gitlab.apertis.org/infrastructure/ansible-playbooks) and
[QA Report App-specific details](https://gitlab.apertis.org/infrastructure/ansible-playbooks/-/tree/main/digital-ocean-k8s/roles/qa-report-app)
for more information.
