from config import config
from taskmanager import TestTask
import struct
from yattag import Doc
from typing import List, Sequence, Tuple

css = """
body {
    max-width: 640px;
}

h1 {
    background-color: #F7F7F7;
    border-top: thin solid #DDE8EF;
    border-bottom: thin solid #DDE8EF;
}

td,
th {
    border: 1px solid rgb(190, 190, 190);
    padding: 10px;
}

tr:nth-child(even) {
    background-color: #eee;
}

table {
    border-collapse: collapse;
    border: 2px solid rgb(200, 200, 200);
    font-family: sans-serif;
}

th {
    text-align: left;
}

a {
    color: #136CB2;
    text-decoration: none;
}

a:hover {
    text-decoration: underline;
}
"""

def generate_md_report(new_tasks: Sequence[TestTask], updated_tasks: Sequence[TestTask]) -> str:
    '''
    Generate a Markdown report based on two lists of task numbers.

    The first list is for newly filed issues, the second one is
    for repeatedly occurring issues that have been commented on.

    Both lists are formatted as tables, with the first column
    for the task numbers and the second column for the task
    descriptions.
    '''
    lines = []
    if new_tasks:
        lines.append('#### New issues')
        lines.append('| Task number | Task title |')
        lines.append('| :---------: | :--------- |')
        for t in new_tasks:
            lines.append('| [T{id}]({url}) | [{title}]({url}) |'.format(
                id=t.task_id,
                url=t.task_url,
                title=t.task_title
            ))
    if updated_tasks:
        lines.append('#### Updated issues')
        lines.append('| Task number | Task title |')
        lines.append('| :---------: | :--------- |')
        for t in updated_tasks:
            lines.append('| [T{id}]({url}) | [{title}]({url}) |'.format(
                id=t.task_id,
                url=t.task_url,
                title=t.task_title
            ))

    return '\n'.join(lines)

def generate_text_report(new_tasks: Sequence[TestTask], updated_tasks: Sequence[TestTask]) -> str:
    '''
    Generate a plain text report based on two lists of task numbers.

    The first list is for newly filed issues, the second one is
    for repeatedly occurring issues that have been commented on.

    Both lists are formatted as tables, with the first column
    for the task numbers and the second column for the task
    descriptions.
    '''
    lines = []
    if new_tasks:
        lines.append('New issues')
        lines.append('----------\n')
        lines.append(' Task number | Task title')
        lines.append(' ----------- | ----------')
        for t in new_tasks:
            lines.append(' {id:^11} | {title}'.format(
                id='T%d' % t.task_id,
                title=t.task_title
            ))
    if updated_tasks:
        lines.append('Updated issues')
        lines.append('--------------\n')
        lines.append(' Task number | Task title')
        lines.append(' ----------- | ----------')
        for t in updated_tasks:
            lines.append(' {id:^11} | {title}'.format(
                id='T%d' % t.task_id,
                title=t.task_title
            ))

    return '\n'.join(lines)


def generate_html_report(new_tasks: Sequence[TestTask], updated_tasks: Sequence[TestTask]) -> str:
    '''
    Generate an HTML report based on two lists of task numbers.

    The first list is for newly filed issues, the second one is
    for repeatedly occurring issues that have been commented on.

    Both lists are formatted as tables, with the first column
    for the task numbers and the second column for the task
    descriptions.
    '''
    doc, tag, text, line = Doc().ttl()
    doc.asis('<!DOCTYPE html>')
    with tag('html'):
        with tag('head'):
            line('style', css)
        with tag('body'):
            if new_tasks:
                line('h1', 'New issues')
                with tag('table'):
                    with tag('thead'):
                        with tag('tr'):
                            line('th', 'Task number', scope='col')
                            line('th', 'Task title', scope='col')
                    with tag('tbody'):
                        for t in new_tasks:
                            with tag('tr'):
                                with tag('td'):
                                    line('a', 'T%d' % t.task_id, href=t.task_url)
                                with tag('td'):
                                    line('a', t.task_title, href=t.task_url)
            if updated_tasks:
                line('h1', 'Updated issues')
                with tag('table'):
                    with tag('thead'):
                        with tag('tr'):
                            line('th', 'Task number', scope='col')
                            line('th', 'Task title', scope='col')
                    with tag('tbody'):
                        for t in updated_tasks:
                            with tag('tr'):
                                with tag('td'):
                                    line('a', 'T%d' % t.task_id, href=t.task_url)
                                with tag('td'):
                                    line('a',t.task_title, href=t.task_url)
    return doc.getvalue()
