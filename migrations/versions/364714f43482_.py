"""Add a tag type column in the starred table

Revision ID: 364714f43482
Revises: 9189604bea95
Create Date: 2022-11-10 19:52:57.742581

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '364714f43482'
down_revision = '9189604bea95'
branch_labels = None
depends_on = None

tag_type = postgresql.ENUM('release', 'weekly', name='tagtype')


def upgrade():
    tag_type.create(op.get_bind())

    # Add the column as nullable
    op.add_column('starred', sa.Column('tag_type', tag_type, nullable=True))

    # Set the tag_type value of all entries to the default (release)
    op.execute("UPDATE starred SET tag_type = 'release'")

    # Set the column as non-nullable
    op.alter_column('starred', 'tag_type', nullable=False)


def downgrade():
    op.drop_column('starred', 'tag_type')

    tag_type.drop(op.get_bind())
