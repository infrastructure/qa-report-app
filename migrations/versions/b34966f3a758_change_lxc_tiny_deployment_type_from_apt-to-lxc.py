"""Change lxc-tiny deployment type from 'apt' to 'lxc'

Revision ID: b34966f3a758
Revises: 6951d2025d13
Create Date: 2019-07-12 06:41:56.750351

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b34966f3a758'
down_revision = '6951d2025d13'
branch_labels = None
depends_on = None


def upgrade():
    connection = op.get_bind()
    connection.execute("""
        UPDATE jobs
           SET image_deployment = 'lxc'
         WHERE image_type = 'tiny-lxc'
    """)

def downgrade():
    connection = op.get_bind()
    connection.execute("""
        UPDATE jobs
           SET image_deployment = 'apt'
         WHERE image_type = 'tiny-lxc'
    """)
