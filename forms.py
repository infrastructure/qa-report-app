###################################################################################
# QA Report Application
#
# Implemention of submission forms model.
#
# Copyright (C) 2019 Collabora Ltd
# Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired, Regexp, ValidationError

from config import ImagesConfig


class SelectImageForm(FlaskForm):

    image_release = StringField('Release', validators=[
        DataRequired(),
        Regexp(
            r'v\d{4}(dev\d|pre)?(\.\d+)?(rc\d+)?',
            message='Invalid release.'
        ),
    ])
    image_version = StringField('Version', validators=[DataRequired()])

    # Choices are added here in webhook.py, when this form is actually used.
    image_deployment = SelectField('Deployment', choices=[])
    image_type = SelectField('Image Type', choices=[])

    def __init__(self, images: ImagesConfig) -> None:
        super().__init__()
        self.images = images

        self.image_deployment.choices = list(
            self.images.deployments.user_selectable.items())

        for platform_group in self.images.platform_groups:
            # Only list the platforms in a group if at least one of the
            # deployments is user-selectable.
            if any(d in self.images.deployments.user_selectable
                    for d in platform_group.deployments):
                self.image_type.choices.extend(platform_group.platforms.items())

    def validate_image_type(self, field):
        platform_group = self.images.find_platform_group_matching_image(
            release=self.image_release.data,
            version=self.image_version.data,
            platform=field.data,
        )
        if platform_group is None:
            raise ValidationError(
                f"Platform is not available for release '{self.image_release.data}'"
                f" and version '{self.image_version.data}'.")

        deployment = self.image_deployment.data
        if deployment not in platform_group.deployments:
            deployment_desc = self.images.deployments.user_selectable[deployment]
            raise ValidationError(
                f"Platform does not support deployment type '{deployment_desc}'."
            )


class SubmitReportForm(FlaskForm):
    # Form is dynamically generated from the submit_image_report.html template.
    pass
