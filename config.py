###################################################################################
# LAVA CI callback webservice
#
# Configuration module
#
# Copyright (C) 2018 Collabora Ltd
# Andrej Shadura <andrew.shadura@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from dataclasses import dataclass
from typing import Generic, Optional, TypeVar
import enum
import dataclasses
import pydantic
import warnings
import yaml

# Pydantic warns about the relationship between aliases and environment
# variables changing, and to silence it, you'd normally just set env=
# explicitly. The problem is that we're using alias_generator, which means we'd
# have to set env= for *every* field. Instead, just silence the warning.
warnings.filterwarnings('ignore', message = 'aliases are no longer used.*')


DEFAULT_CHAT_TEMPLATE = """
### Test failures for image {image_version}

|                | Phabricator tasks      | Architectures    |
| :------------- | ----------------------:| :---------------:|
| New issues     | {created_tasks_number} | {created_arches} |
| Updated issues | {updated_tasks_number} | {updated_arches} |

{report}
"""


DEFAULT_COMMENT_TEMPLATE = """
Test [{case}]({app_url}{url_prefix}/{image_release}/{case}.html) failed on [{image_release}-{image_version}]({app_url}{url_prefix}/report/{image_release}/{image_version}).
Image types: {image_types}

Links to logs:
{logs}
"""


DEFAULT_EMAIL_SUBJECT_TEMPLATE = """
LAVA test failures for image {image_release}-{image_version}
"""


DEFAULT_TASK_TEMPLATE = """
Affected image versions
=======================
{image_versions}

Testcase
========
[{test_case_name}]({app_url}{url_prefix}/{image_release}/{test_case_name}.html)

Test case priority
==================
{test_priority}

Reproducibility
===============
TBD

Impact of bug
=============
TBD

Attachments
===========
{logs}

Root cause
==========
TBD

Outcomes
========
TBD

Management data
===============
This section is for management only, it should be the last one in the description

apertis:lava:suite: {test_case_name}
"""


DEFAULT_IMAGES_CONFIG = {
    'architectures': ['armhf', 'arm64', 'amd64'],
    'variants': [
        'minimal',
        'target',
        'fixedfunction',
        'hmi',
        'basesdk',
        'sdk',
        'tiny-lxc',
        'nfsroot',
    ],
    'deployments': {
        'user-selectable': {
            'apt': 'APT',
            'ostree': 'OSTree',
        },
        'lava-only': {'nfs', 'lxc'},
    },
    'platform-groups': [
        {
            'platforms': {
                'nfsroot-armhf-uboot-public': 'Nfsroot ARMhf (uboot)',
                'nfsroot-arm64-uboot-public': 'Nfsroot ARM64 (uboot)',
                'nfsroot-amd64-uefi-public': 'Nfsroot AMD64',
            },
            'deployments': {'nfs'},
        },
        {
            'platforms': {
                'tiny-lxc-armhf-uboot-public': 'Tiny LXC ARMhf (uboot)',
                'tiny-lxc-arm64-uboot-public': 'Tiny LXC ARM64 (uboot)',
                'tiny-lxc-amd64-uefi-public': 'Tiny LXC AMD64',
            },
            'deployments': {'lxc'},
        },
        {
            'platforms': {
                'fixedfunction-armhf-uboot-public': 'Fixed function ARMhf (uboot)',
                'fixedfunction-arm64-uboot-public': 'Fixed function ARM64 (uboot)',
                'fixedfunction-arm64-rpi64-public': 'Fixed function ARM64 (rpi64)',
                'fixedfunction-amd64-uefi-public': 'Fixed function AMD64',
            },
            'deployments': {'apt', 'ostree'},
            'supported-by': [
                {
                    'first': {
                        'release': 'v2022dev3',
                    }
                }
            ]
        },
        {
            'platforms': {
                'minimal-armhf-uboot-public': 'Minimal ARMhf (uboot)',
                'minimal-arm64-uboot-public': 'Minimal ARM64 (uboot)',
                'minimal-amd64-uefi-public': 'Minimal AMD64',
            },
            'deployments': {'apt', 'ostree'},
            'supported-by': [
                {
                    'last': {
                        'release': 'v2022dev2',
                    }
                }
            ]
        },
        {
            'platforms': {
                'hmi-armhf-uboot-public': 'HMI ARMhf (uboot)',
                'hmi-arm64-rpi64-public': 'HMI ARM64 (rpi64)',
                'hmi-amd64-uefi-public': 'HMI AMD64',
            },
            'deployments': {'apt', 'ostree'},
            'supported-by': [
                {
                    'first': {
                        'release': 'v2022dev3',
                    },
                }
            ]
        },
        {
            'platforms': {
                'target-armhf-uboot-public': 'Target ARMhf (uboot)',
            },
            'deployments': {'apt', 'ostree'},
            'supported-by': [
                {
                    'last': {
                        'release': 'v2019',
                        'inclusive': False,
                    }
                },
                {
                    'first': {
                        'release': 'v2019',
                        'inclusive': False,
                    },
                    'last': {
                        'release': 'v2022dev2',
                    },
                },
            ],
        },
        {
            'platforms': {
                'target-amd64-uefi-public': 'Target AMD64',
            },
            'deployments': {'apt', 'ostree'},
            'supported-by': [
                {
                    'last': {
                        'release': 'v2022dev2',
                    },
                }
            ],
        },
        {
            'platforms': {
                'basesdk-amd64-sdk-public': 'BaseSDK AMD64',
                'sdk-amd64-sdk-public': 'SDK AMD64',
            },
            'deployments': {'apt'},
        },
    ],
}


@dataclass
class TemplateArgs:
    def to_dict(self) -> dict:
        return dataclasses.asdict(self)


_TemplateArgsVar = TypeVar('_TemplateArgsVar', bound=TemplateArgs)


@dataclass
class Template(Generic[_TemplateArgsVar]):
    """
    A string template parsed from the config file. _TemplateArgsVar is a
    dataclass whose to_dict() method should return a mapping of all the values
    to substitute into the template.
    """

    template: str

    @classmethod
    def _get_test_substitution(cls) -> '_TemplateArgsVar':
        """
        Returns an "empty" instantiation of _TemplateArgsVar, to be used to run
        a test substitution of the template string as a test of validity.
        """

        raise NotImplementedError()

    @classmethod
    def __get_validators__(cls):
        yield cls.__validate

    @classmethod
    def __modify_schema__(cls, field_schema):
        pass

    @classmethod
    def __validate(cls, value):
        if isinstance(value, cls):
            # This is a default value, but check the contents anyway.
            value = value.template

        if not isinstance(value, str):
            raise TypeError('template must be a string')

        test_substitution = cls._get_test_substitution()
        try:
            cls._substitute(value, test_substitution)
        except KeyError as ex:
            raise ValueError(f"invalid template variable '{ex.args[0]}'")

        return cls(template=value)

    @staticmethod
    def _substitute(template: str, args: _TemplateArgsVar) -> str:
        return template.format(**args.to_dict())

    def substitute(self, args: _TemplateArgsVar) -> str:
        return self._substitute(self.template, args)


def to_kebab_case(s: str) -> str:
    return s.replace('_', '-')


class BugTrackingConfig(enum.Enum):
    NONE = 'none'
    PHABRICATOR = 'phabricator'
    GITLAB = 'gitlab'


class GitlabIssuesConfig(pydantic.BaseModel):
    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid

    filter_label: str = 'area:test-failure'
    labels: list[str] = []


class DbConfig(pydantic.BaseModel):
    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid

    host: str
    name: str
    password: str
    user: str
    port: int = 5432

    @property
    def uri(self) -> str:
        return (
            f'postgresql://{self.user}:{self.password}'
            + f'@{self.host}:{self.port}/{self.name}'
        )


class Display(pydantic.BaseModel):
    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid

    class SeriesCount(pydantic.BaseModel):
        class Config:
            alias_generator = to_kebab_case
            extra = pydantic.Extra.forbid

        dailies: int
        releases: int
        weeklies: int

    min_version: str
    series_results: SeriesCount


@dataclass
class CommonNotificationTemplateArgs(TemplateArgs):
    created_arches: str
    updated_arches: str
    created_tasks_number: int
    updated_tasks_number: int
    image_release: str
    image_version: str

    @classmethod
    def get_test_substitution(cls) -> 'CommonNotificationTemplateArgs':
        return cls(
            created_arches='',
            updated_arches='',
            created_tasks_number=0,
            updated_tasks_number=0,
            image_release='',
            image_version='',
        )


class NotificationsConfig(pydantic.BaseModel):
    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid

    class Chat(pydantic.BaseModel):
        class Config:
            alias_generator = to_kebab_case
            extra = pydantic.Extra.forbid

        @dataclass
        class TextTemplateArgs(TemplateArgs):
            report: str
            common: CommonNotificationTemplateArgs

            def to_dict(self) -> dict:
                data = super().to_dict()
                data.update(data.pop('common'))
                return data

        class TextTemplate(Template[TextTemplateArgs]):
            @classmethod
            def _get_test_substitution(cls):
                return NotificationsConfig.Chat.TextTemplateArgs(
                    report='',
                    common=CommonNotificationTemplateArgs.get_test_substitution(),
                )

        url: str
        icon_url: Optional[str] = None
        text_template: TextTemplate = TextTemplate(DEFAULT_CHAT_TEMPLATE)
        to: list[str] = []
        username: str = 'LAVA'

    class Email(pydantic.BaseModel):
        class Config:
            alias_generator = to_kebab_case
            extra = pydantic.Extra.forbid

        @dataclass
        class SubjectTemplateArgs(TemplateArgs):
            common: CommonNotificationTemplateArgs

            def to_dict(self) -> dict:
                data = super().to_dict()
                data.update(data.pop('common'))
                return data

        class SubjectTemplate(Template[SubjectTemplateArgs]):
            @classmethod
            def _get_test_substitution(cls):
                return NotificationsConfig.Email.SubjectTemplateArgs(
                    common=CommonNotificationTemplateArgs.get_test_substitution(),
                )

        from_: str = pydantic.Field('noreply@lavaphabbridge', alias='from')
        subject_template: SubjectTemplate = SubjectTemplate(
            DEFAULT_EMAIL_SUBJECT_TEMPLATE.strip()
        )
        to: list[str]
        server: str = 'localhost'
        port: int = 25
        user: Optional[str] = None
        password: Optional[str] = None

    chat: Optional[Chat] = None
    email: Optional[Email] = None


class ImagesConfig(pydantic.BaseModel):
    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid

    class Deployments(pydantic.BaseModel):
        class Config:
            alias_generator = to_kebab_case
            extra = pydantic.Extra.forbid

        user_selectable: dict[str, str] = {}
        lava_only: set[str] = set()

        @property
        def all_names(self) -> set[str]:
            return set(self.user_selectable) | self.lava_only

        @pydantic.validator('lava_only')
        def validate_lava_only(cls, lava_only: list[str], values):
            if (user_selectable := values.get('user_selectable')) is not None:
                for deployment in lava_only:
                    if deployment in user_selectable:
                        raise ValueError(f"duplicate deployment '{deployment}'")

            return lava_only

    class PlatformGroup(pydantic.BaseModel):
        class SupportRange(pydantic.BaseModel):
            class Constraint(pydantic.BaseModel):
                release: str
                version: Optional[str] = None
                inclusive: bool = pydantic.Field(True)

                def equals(self, *, release: str, version: Optional[str]) -> bool:
                    if self.release != release:
                        return False

                    if self.version is not None and version is not None:
                        return self.version == version

                    return True

                def older_than(self, *, release: str, version: Optional[str]) -> bool:
                    if self.release != release:
                        # v2022pre is older than v2022.
                        if self.release.startswith(release):
                            return True
                        elif release.startswith(self.release):
                            return False

                    if self.release == release:
                        if self.version is not None and version is not None:
                            return self.version < version
                        else:
                            return False

                    return self.release < release

            first: Optional[Constraint] = None
            last: Optional[Constraint] = None

            @pydantic.root_validator
            def validate_first_last(cls, values):
                first = values.get('first')
                last = values.get('last')

                if first is None and last is None:
                    raise ValueError(f"At least one of 'first' and 'last' must be set")
                elif first is not None and last is not None:
                    if last.older_than(release=first.release, version=first.version):
                        raise ValueError('Last version is older than first')

                    if (first.release == last.release
                            and (first.version is None or last.version is None)):
                        raise ValueError('First and last versions overlap.')

                return values

            def includes(self, *, release: str, version: Optional[str]) -> bool:
                if self.first is not None:
                    if self.first.equals(release=release, version=version):
                        if self.first.version is not None and version is None:
                            return True
                        elif not self.first.inclusive:
                            return False
                    elif not self.first.older_than(release=release, version=version):
                        return False

                if self.last is not None:
                    if self.last.equals(release=release, version=version):
                        if self.last.version is not None and version is None:
                            return True
                        elif not self.last.inclusive:
                            return False
                    elif self.last.older_than(release=release, version=version):
                        return False

                return True

        class Config:
            alias_generator = to_kebab_case
            extra = pydantic.Extra.forbid

        platforms: dict[str, str]
        deployments: set[str]
        supported_by: list[SupportRange] = pydantic.Field([])

        def matches(self, *, release: str, version: Optional[str]) -> bool:
            if not self.supported_by:
                return True

            return any(support_range.includes(release=release, version=version)
                        for support_range in self.supported_by)

    architectures: set[str]
    variants: set[str]
    deployments: Deployments
    platform_groups: list[PlatformGroup]

    @pydantic.validator('platform_groups')
    def validate_platform_groups(cls, platform_groups: list[PlatformGroup], values):
        architectures = values.get('architectures')
        variants = values.get('variants')
        deployments = values.get('deployments')

        platforms_seen = set()

        for group in platform_groups:
            new_platforms = set(group.platforms)
            duplicate_platforms = platforms_seen & new_platforms
            if duplicate_platforms:
                first_duplicate = next(iter(duplicate_platforms))
                raise ValueError(f"duplicate platform '{first_duplicate}'")

            if variants is not None:
                for platform in group.platforms:
                    for variant in variants:
                        variant_prefix = f'{variant}-'
                        if platform.startswith(variant_prefix):
                            break
                    else:
                        raise ValueError(f"platform '{platform}' uses unknown variant")

                    if architectures is not None:
                        platform_without_variant = platform.removeprefix(variant_prefix)
                        arch_prefixes = tuple(f'{arch}-' for arch in architectures)

                        if not platform_without_variant.startswith(arch_prefixes):
                            raise ValueError(f"platform '{platform}' uses unknown arch")

            if deployments is not None:
                unknown_deployments = group.deployments - deployments.all_names
                if unknown_deployments:
                    raise ValueError('platform group uses unknown deployments:'
                            f" '{', '.join(unknown_deployments)}'")

        return platform_groups

    def find_platform_group_matching_image(self,
        release: str,
        version: str,
        platform: str,
    ):
        for platform_group in self.platform_groups:
            if (platform in platform_group.platforms
                    and platform_group.matches(release=release, version=version)):
                return platform_group

        return None


class PhabricatorTasksConfig(pydantic.BaseModel):
    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid

    space: str = 'S2'
    filter_tag: str = 'test-failure'
    tags: list[str] = ['bug']


class TestCasesRepositoryConfig(pydantic.BaseModel):
    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid

    url: str = 'https://gitlab.apertis.org/tests/apertis-test-cases.git'
    branch_prefix: str = 'apertis/'


class OpenIdManualConfig(pydantic.BaseModel):
    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid

    issuer: str
    jwks_uri: str

    # mandatory endpoints
    authorization_endpoint: str
    token_endpoint: str
    userinfo_endpoint: str

    # optional endpoints
    introspection_endpoint: Optional[str] = None
    revocation_endpoint: Optional[str] = None


class OpenId(pydantic.BaseModel):
    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid

    always_require_login: Optional[bool] = False
    client_id: str
    client_secret: str
    manual: Optional[OpenIdManualConfig] = None
    well_known_url: Optional[str] = None
    token_refresh_interval: int = 0

    @pydantic.root_validator
    def _check_manual_or_auto(cls, values):
        if values['manual'] is None and values['well_known_url'] is None:
            raise ValueError(
                "Either 'manual' or 'well-know-url' must be set")
        elif values['manual'] is not None and values['well_known_url'] is not None:
            raise ValueError(
                "Only one of 'manual' or 'well-know-url' must be set")

        return values


class AuthGroupPermission(enum.Enum):
    SUBMIT_REPORTS = 'submit-reports'
    TAG_IMAGES = 'tag-images'


class AuthGroup(pydantic.BaseModel):
    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid

    name: str
    extra_perms: list[AuthGroupPermission] = []

    def has_permission(self, perm) -> bool:
        return perm in self.extra_perms


class AppConfig(pydantic.BaseSettings):
    @dataclass
    class CommentTemplateArgs(TemplateArgs):
        app_url: str
        url_prefix: str
        case: str
        image_release: str
        image_releases: list[str] # for compatibility
        image_version: str
        image_types: str
        logs: str

    class CommentTemplate(Template[CommentTemplateArgs]):
        @classmethod
        def _get_test_substitution(cls) -> 'AppConfig.CommentTemplateArgs':
            return AppConfig.CommentTemplateArgs(
                app_url='',
                url_prefix='',
                case='',
                image_release='',
                image_releases=[],
                image_version='',
                image_types='',
                logs='',
            )

    @dataclass
    class TaskTemplateArgs(TemplateArgs):
        app_url: str
        url_prefix: str
        image_release: str
        image_version: str
        image_versions: str
        logs: str
        test_case_name: str
        test_priority: str

    class TaskTemplate(Template[TaskTemplateArgs]):
        @classmethod
        def _get_test_substitution(cls) -> 'AppConfig.TaskTemplateArgs':
            return AppConfig.TaskTemplateArgs(
                app_url='',
                url_prefix='',
                image_release='',
                image_version='',
                image_versions='',
                logs='',
                test_case_name='',
                test_priority='',
            )

    class Config:
        alias_generator = to_kebab_case
        extra = pydantic.Extra.forbid
        env_nested_delimiter = '_'

    app_url: str = 'https://qa.apertis.org'
    url_prefix: str = ''
    auth_groups: list[AuthGroup] = []
    bug_tracking: BugTrackingConfig = BugTrackingConfig.PHABRICATOR
    comment_template: CommentTemplate = CommentTemplate(DEFAULT_COMMENT_TEMPLATE)
    db: DbConfig
    debug: bool = False
    display: Optional[Display] = None
    flask_secret_key: str = ''
    gitlab_issues: GitlabIssuesConfig = GitlabIssuesConfig()
    gitlab_project: str = 'infrastructure/apertis-issues'
    qa_report_gitlab_token: Optional[str] = None
    gitlab_url: str = 'https://gitlab.apertis.org'
    image_root: str = 'https://images.apertis.org'
    images: ImagesConfig = ImagesConfig.parse_obj(DEFAULT_IMAGES_CONFIG)
    lava_callback_tokens: list[str] = []
    lava_url: str = 'https://lava.collabora.dev'
    log_file: Optional[str] = None
    notifications: NotificationsConfig = NotificationsConfig()
    openid: Optional[OpenId] = None
    port: int = 28080
    phabricator_tasks: PhabricatorTasksConfig = PhabricatorTasksConfig()
    phabricator_token: Optional[str] = None
    phabricator_url: str = 'https://phabricator.apertis.org'
    proxy_count: int = 0
    test_cases_repository: TestCasesRepositoryConfig = TestCasesRepositoryConfig()
    task_template: TaskTemplate = TaskTemplate(DEFAULT_TASK_TEMPLATE)

    @pydantic.root_validator
    def _validate_config(cls, values):
        if values['url_prefix'] and not values['url_prefix'].startswith('/'):
            raise ValueError(
                'url-prefix must start with a leading slash')

        if not config.skip_bug_tracking_validation:
            if (values['bug_tracking'] == BugTrackingConfig.PHABRICATOR
                    and not values['phabricator_token']):
                raise ValueError(
                    'phabricator-token is required for Phabricator integration')
            elif (values['bug_tracking'] == BugTrackingConfig.GITLAB
                    and not values['qa_report_gitlab_token']):
                raise ValueError(
                    'qa-report-gitlab-token is required for GitLab Issues integration')

        return values


class ConfigContainer(object):
    def __init__(self) -> None:
        self._instance = None
        self.skip_bug_tracking_validation = False

    def load_config_from_file(self, path: str):
        with open(path) as fp:
            data = yaml.safe_load(fp)

        self._instance = AppConfig.parse_obj(data)

    def load_config_from_env_only(self):
        self._instance = AppConfig()

    @property
    def instance(self) -> AppConfig:
        assert self._instance is not None, 'load_config has not yet been called!'
        return self._instance


config = ConfigContainer()
