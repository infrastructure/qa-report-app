###################################################################################
# Report pages generator.
#
# Copyright (C) 2019 Collabora Ltd
# Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import itertools
import re
import html
import logging
import textwrap

from flask import  request, render_template
from models import Job, TestCase, Starred, reports_overview, TagType
from config import config
from sqlalchemy import desc
from sqlalchemy.orm import contains_eager
from packaging import version

priority_color_table = {
    'low'      : 'secondary',
    'medium'   : 'info',
    'high'     : 'warning',
    'critical' : 'danger'
}

def _retrieve_previous_notes(test_case, image_release, image_deployment, image_build, visibility):
    if not re.match(r'^v(\d{4})(?!\d)', image_release):
        return None

    matching_test_cases = (TestCase.query
        .join(Job)
        .options(contains_eager(TestCase.job))
        .filter(TestCase.suite == test_case,
                TestCase.result != 'na',
                TestCase.result != 'not_tested',
                Job.image_release.startswith(image_release[:5]),
                Job.image_deployment == image_deployment,
                Job.visibility == visibility)
        .order_by(Job.creation_time.desc()))

    latest_version = None
    last_matching_test_case = None

    for tc in matching_test_cases:
        # Take the notes of this TC if the tc's job version is < than the version being added
        if version.parse(tc.job.image_release) < version.parse(image_release) and \
            (not latest_version or \
                (version.parse(latest_version) < version.parse(tc.job.image_release))) :
            latest_version = tc.job.image_release
            last_matching_test_case = tc

    if (not last_matching_test_case
            or not (notes := last_matching_test_case.notes)
            or not (notes := notes.strip())):
        return None

    # Strip off any previous import information.
    notes = re.sub(r'(?i)^Imported\s+from\s+\S+\s*:\s*', '', notes)

    origin_testcase = '/'.join((
        last_matching_test_case.job.image_release,
        last_matching_test_case.job.image_build,
        last_matching_test_case.job.image_deployment,
    ))

    notes = f'Imported from {origin_testcase}:\n{notes}'

    return { last_matching_test_case.suite: (None, notes) }


def _format_notes(notes):
    apertis_match = re.compile(r'\bAPERTIS-T(?P<task>\d+)\b')
    http_match = re.compile(r'\bhttp(s)?://(\S+)\b')
    apertis_link = r'<a href="{}/T\g<task>">\g<0></a>'
    http_link = r'<a href="\g<0>">\g<0></a>'

    apertis_task_link = apertis_link.format(config.instance.phabricator_url)

    notes = http_match.sub(http_link, notes)
    notes = apertis_match.sub(apertis_task_link, notes)
    return f'<div>{notes}</div>'


def _keep_latest(jobs, max_count):
    grouped_jobs = itertools.groupby(
        sorted(jobs, reverse=True, key=lambda x: (x.image_release[:5], x.image_build)),
        key=lambda x: x.image_release[:5])

    new_jobs = []

    for release, release_jobs in grouped_jobs:
        new_jobs += list(release_jobs)[:max_count]

    new_jobs = sorted(new_jobs, reverse=True, key=lambda x: (x.image_build))

    return new_jobs


def generate_index(testcases_table, username=None, user_can_submit_reports=False, show_all=False):
    entries = reports_overview(show_all=show_all)

    released = [e for e in entries if e['tag_type'] == 'release']
    weeklies = [e for e in entries if e['tag_type'] == 'weekly']
    builds = [e for e in entries if not e['tag_type']]

    if not show_all and config.instance.display is not None:
        # Sort tagged releases to get only the last max_count tags for each image_release versions.
        # e.g.: show results for
        # * v2023pre+v2023dev3 (last 2 release results in the v2023 series)
        # * v2022.2+v2022.1
        # * v2021.6+v2021.5
        max_count = config.instance.display.series_results.releases
        released = _keep_latest(released, max_count)

        # Sort untagged releases (weeklies and dailies)
        max_count = config.instance.display.series_results.dailies
        builds = _keep_latest(builds, max_count)

        max_count = config.instance.display.series_results.weeklies
        weeklies = _keep_latest(weeklies, max_count)

    def release_filter(key):
        return show_all or \
            config.instance.display is None or \
            version.parse(key) >= version.parse(config.instance.display.min_version)

    all_releases = sorted({k for k in testcases_table.get() if release_filter(k)},
                          reverse=True)

    # Only include development/pre-releases preceding a stable release for the
    # latest one. Older stable releases will only have the stable release itself
    # shown, not its development/pre-releases.
    releases_to_display = []
    added_at_least_one_stable_release = False
    for release in all_releases:
        if releases_to_display and releases_to_display[-1].startswith(release):
            # For the most recent stable release, show a handful of pre-releases
            # leading up to it. For any older stable releases, don't show *any*
            # pre-releases, to keep things condensed.
            prereleases_to_keep = 0 if added_at_least_one_stable_release else 3

            # Go backwards through the current releases to find the first
            # prerelease.
            prerelease_indexes = itertools.takewhile(
                lambda i: releases_to_display[i].startswith(release),
                reversed(range(len(releases_to_display))))
            try:
                # The indexes were reversed, so the first/latest pre-release in
                # the list is actually the *last* item of our iterator.
                *_, first_prerelease = prerelease_indexes
            except ValueError:
                # No pre-releases, so nothing to do.
                pass
            else:
                prereleases_to_keep = min(prereleases_to_keep,
                    len(releases_to_display) - first_prerelease)
                prereleases_present = len(releases_to_display) - first_prerelease
                prereleases_to_delete = prereleases_present - prereleases_to_keep
                del releases_to_display[-prereleases_to_delete:]

                # If there is at least one pre-release left, insert the stable
                # release before it, so that it comes up first in the rendered
                # template.
                if prereleases_present > prereleases_to_delete:
                    releases_to_display.insert(first_prerelease, release)
                    added_at_least_one_stable_release = True

                    # We already inserted this release, so just keep going.
                    continue

        releases_to_display.append(release)

    return render_template('index.html',
                           username=username,
                           user_can_submit_reports=user_can_submit_reports,
                           url=request.base_url,
                           released=released,
                           weeklies=weeklies,
                           builds=builds,
                           releases=releases_to_display,
                           show_all=show_all)

def generate_select_image(username, form):
    return render_template('select_image.html', username=username, form=form)

def generate_submit_report(username, form, image_release,
                           image_version, image_deployment,
                           image_type, manual_testcases_table):

    # Search if there is already a saved job for this image type and show the
    # values in the submit_report page.
    img_type, img_arch, img_platform, visibility = image_type.split('-')
    job_db = Job.query.filter_by(image_release      = image_release,
                                 image_build        = image_version,
                                 image_deployment   = image_deployment,
                                 image_type         = img_type,
                                 image_architecture = img_arch,
                                 image_platform     = img_platform,
                                 visibility         = visibility,
                                 exec_type          = "manual").first()
    records = {}
    if job_db:
        # Build a structure of the form { testcase: (result, notes) .. } and pass
        # to the template to render existing values in the submit_report page.
        for testcase_record in job_db.testcases:
            records.update({ testcase_record.suite: (testcase_record.result,
                                                     testcase_record.notes) })
    else:
        # If this isn't an update, try to find matching notes from the last job
        # of the same release and deployment, and fill those in for each test
        # case.
        for test_case, *_ in manual_testcases_table:
            if (notes := _retrieve_previous_notes(test_case, image_release, \
                    image_deployment, image_version, visibility)):
                records.update(notes)

    return render_template('submit_report.html',
                           username=username,
                           image_release=image_release,
                           image_version=image_version,
                           image_type=image_type,
                           testcases_table=manual_testcases_table,
                           priority_color_table=priority_color_table,
                           records=records,
                           form=form)

def _create_report_table(test_cases, testcases_table, image_release,
                         image_version, image_deployment, show):
    """
    This method generates the main report table passed to the html template.
    It does so by processing the saved `test_cases` results against the
    `testcases_table` data.

    It generates a structure of the form:

      [ (suite, priority, notes, { image_type: (result, url) ... }),  ... ]
    """
    report_table = []
    is_alltests = True

    supported_platforms = {}
    supported_platform_indexes = {}

    for suite, exec_type, priority, deployments, platforms in testcases_table:
        suite_images = {}
        # Don't add any test case row if deployment is not valid.
        if image_deployment not in deployments:
            continue

        # For each platform we know of, determine if it was tested, not tested,
        # or isn't supported for this test case.
        for i, platform_group in enumerate(config.instance.images.platform_groups):
            if (not platform_group.matches(release=image_release, version=image_version)
                    or image_deployment not in platform_group.deployments):
                continue

            for j, (platform, name) in enumerate(platform_group.platforms.items()):
                if platform in platforms:
                    if suite in test_cases and platform in test_cases[suite]:
                        suite_images[platform] = test_cases[suite][platform]
                    else:
                        suite_images[platform] = ('not_tested', '')

                    supported_platforms[platform] = name
                    supported_platform_indexes[platform] = (i, j)
                else:
                    # Platform is not supported , so mark as N/A.
                    suite_images[platform] = ('na', '')

        # Add notes (if any) for all the platforms of a suite (test case)
        notes = ''
        if suite in test_cases:
            notes = test_cases[suite].get('__notes', '')

        # Filter only executed test cases.
        # This will only show test cases that have been executed at least
        # for one platform in the report page.
        if show == 'run' and all(r == 'not_tested' or r == 'na' \
                                 for (r, _) in suite_images.values()):
            is_alltests = False
            continue
        else:
            report_table.append((suite, priority, notes, suite_images, exec_type))

    # Rebuild the supported platforms in order of their positions in the
    # configuration.
    supported_platforms = dict(sorted(supported_platforms.items(),
        key=lambda pair: supported_platform_indexes[pair[0]]))

    return (report_table, supported_platforms, is_alltests)

def generate_report(image_release, image_version, image_deployment,
                    show, testcases_table, username, user_can_tag_images):
    logging.info('Generating report for release: %s , build: %s',
                 image_release, image_version)

    # Generate a `test_cases` structure of the form:
    # test_cases = {
    #     'boot-test' : {
    #         'target-amd64-uefi-public' : ('pass', lava_url),
    #         'target-armhf-uboot-internal' : ('pass', lava_url)
    #         ....
    #         '__notes': "boot-test notes",
    #     },
    #     'gui-test' : {
    #         'target-amd64-uefi-public' : ('pass', lava_url),
    #         'target-armhf-uboot-internal' : ('failed', lava_url)
    #         ....
    #     }
    #     .....
    # }
    test_cases = {}
    image_urls = {}
    automated_count = 0
    manual_count = 0
    entries = reports_overview()
    released = [e for e in entries if e['tag_type'] == 'release']
    weeklies = [e for e in entries if e['tag_type'] == 'weekly']
    builds = [e for e in entries if not e['tag_type']]

    # Check if this report belongs to a release
    starred_job = \
        Starred.query.filter_by(image_release = image_release,
                                image_version = image_version).one_or_none()

    for job in Job.query.filter_by(image_release=image_release,
                                   image_build=image_version,
                                   image_deployment=image_deployment).order_by(Job.creation_time).all():
        # target-amd64-uefi-public , target-armhf-uboot-private ....
        image_platform = job.infer_image_platform()
        image_type_id = '-'.join([ job.image_type,
                                   job.image_architecture,
                                   image_platform,
                                   job.visibility ])

        platform_group = config.instance.images.find_platform_group_matching_image(
                release=image_release,
                version=image_version,
                platform=image_type_id)
        if platform_group is None or image_deployment not in platform_group.deployments:
            logging.warning(textwrap.dedent(f'''
                Found job for invalid image:
                - {image_release=}
                - {image_deployment=}
                - {image_version=}
                - {image_type_id=}
                - {job.id=}
                - {job.lava_job_id=}
            '''.lstrip('\n')))

        for test_case in job.testcases:
            # Manual tests have no lava_url
            lava_url = '' if job.exec_type == 'manual' else \
                       config.instance.lava_url + test_case.lava_url

            test_cases.setdefault(test_case.suite, {}) \
                      .update({ image_type_id: (test_case.result, lava_url) })

            if test_case.notes and len(test_case.notes.strip()) > 0:
                # Append new notes if there are already notes available for this
                # test case.
                escaped_notes = _format_notes(html.escape(test_case.notes))
                if '__notes' in test_cases[test_case.suite]:
                    test_cases[test_case.suite]['__notes'] += escaped_notes
                else:
                    test_cases[test_case.suite]['__notes'] = escaped_notes

        # Skip reporting private/internal images urls.
        if job.visibility == 'public' and job.image_url:
            if starred_job and str(starred_job.tag_type) == "release":
                image_urls[image_type_id] = job.image_url \
                                               .replace("daily", "release") \
                                               .replace(image_version, starred_job.tag)
            elif starred_job and str(starred_job.tag_type) == "weekly":
                image_urls[image_type_id] = job.image_url.replace("daily", "weekly")
            else:
                image_urls[image_type_id] = job.image_url

    # Create report table
    (report_table, supported_platforms, is_alltests) = _create_report_table(
        test_cases, testcases_table[image_release],
        image_release, image_version, image_deployment, show)

    # Remove any image URLs for unsupported platforms.
    for unsupported_platform in set(image_urls.keys()) - set(supported_platforms):
        del image_urls[unsupported_platform]

    is_checked, tag, tag_type = '', None, None
    if starred_job:
        is_checked = 'checked'
        tag = starred_job.tag
        tag_type = str(starred_job.tag_type)

    for test_case, priority, notes, image_types, exec_type in report_table:
        if exec_type == 'automated':
            automated_count += 1
        elif exec_type == 'manual':
            manual_count += 1

    return render_template('report.html',
                           report_table=report_table,
                           supported_platforms=supported_platforms,
                           image_urls=image_urls,
                           image_release=image_release,
                           image_version=image_version,
                           image_deployment=image_deployment,
                           released=released,
                           weeklies=weeklies,
                           builds=builds,
                           starred_job=starred_job,
                           priority_color_table=priority_color_table,
                           is_alltests=is_alltests, is_checked=is_checked,
                           username=username,
                           user_can_tag_images=user_can_tag_images,
                           tag=tag, tag_type=tag_type,
                           automated_count=automated_count, manual_count=manual_count)

def generate_forbidden(error):
    return render_template('403.html', error=error)

def generate_internal_server_error(error):
    return render_template('500.html', error=error)
