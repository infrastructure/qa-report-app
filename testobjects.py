###################################################################################
# LAVA CI callback webservice
#
# Classes for test jobs and test results
#
# Copyright (C) 2018 Collabora Ltd
# Andrej Shadura <andrew.shadura@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import argparse
import json
import yaml
import os.path
from datetime import date, datetime
from collections import namedtuple
import decimal
import sys

def dict_subset(dict_in, keys):
    """Copy only the selected keys to a new dict"""
    dict_out = {}
    for key in keys:
        if key in dict_in:
            dict_out[key] = dict_in[key]
    return dict_out

def lava_jobdata_fixup(data):
    """Convert the YAML sections of the data provided by LAVA to JSON"""
    jobdata = dict(data)
    if 'definition' in jobdata:
        jobdata['definition'] = yaml.safe_load(data['definition'])
    if 'results' in jobdata:
        jobdata['results'] = { key: yaml.safe_load(value) for key, value in data['results'].items() }
    return jobdata

def lava_jobdata_extract_definition(data):
    definition = data['definition']
    """Extract some useful bits from the LAVA job definition"""
    # TODO this is largely about working around the fact that LAVA always sends
    # an empty list as the toplevel metadata field regardless of the job
    # definition, see https://phabricator.collabora.com/T13760
    ret = {}
    ret['device_type'] = definition['device_type']
    ret['visibility'] = definition['visibility']

    # FIXME: make apertis-test pass all the image.* entries as metadata so we can drop the whole block below
    # currently lava_submit.py passes some of those in the registration step but we should move away from it
    image_url = next((i for i in (a.get('deploy',{}).get('images',{}).get('image',{}).get('url') for a in definition['actions']) if i), None)
    if not image_url:
        image_url = next((i for i in (a.get('deploy',{}).get('images',{}).get('rootfs',{}).get('url') for a in definition['actions']) if i), None)
    if not image_url:
        image_url = next((i for i in (a.get('deploy',{}).get('nfsrootfs',{}).get('url') for a in definition['actions']) if i), None)

    # The image URL should to point to Tiny RFS ospack under the test instead of host image for Tiny container report
    if definition['metadata']['image.type'] == 'tiny-lxc' and definition['metadata']['ospack.url']:
        image_url = definition['metadata']['ospack.url']

    ret['image.url'] = image_url
    ret['image.name'] = os.path.basename(image_url)
    # assume the first part of the image name, before any '-' or '_', is the osname
    ret['image.osname'] = ret['image.name'].split('-')[0].split('_')[0]
    ret['image.deployment'] = 'apt'
    ret['image.flavour'] = ''
    if 'ostree' in image_url:
        ret['image.deployment'] = 'ostree'
        ret['image.flavour'] = 'ostree'
    elif 'nfs' in image_url:
        ret['image.deployment'] = 'nfs'
        ret['image.flavour'] = 'nfs'
    elif definition['metadata']['image.type'] == 'tiny-lxc':
        ret['image.deployment'] = 'lxc'
        ret['image.flavour'] = 'tiny'

    ret.update(definition['metadata'])

    return { 'metadata': ret }

def lava_jobdata_aggregate_result_values(results):
    result = 'pass'
    if any(i['result'] == 'fail' for i in results):
        result = 'fail'
    if all(i['result'] == 'skip' for i in results):
        result = 'skip'
    return result

def lava_jobdata_extract_results(data):
    """Extract testcases and results from the data provided by LAVA

    The extracted testcases approximately match the YAML testcase definitions,
    and each carries one or more results.
    """

    actions = {}
    for action in data['results']['lava']:
        if action.get('metadata', {}).get('case') == 'git-repo-action':
            # LAVA gives us an array of dicts/JSON object with a single item inside
            # make it a single dict to be able to use it sanely
            extra = { k: v for e in action['metadata']['extra'] for k, v in e.items() }
            actions[extra['repository'], extra['path']] = (action, extra)

    suites = {}
    for suitename, s in data['results'].items():
        if suitename == 'lava':
            continue
        results = [dict_subset(r, ('id', 'name', 'result', 'url')) for r in s]
        # the result value in the original entry often fails to report the
        # actual presence of errors, so aggregrate the result values from all
        # the sub-results to provide a more accurate overview
        result = lava_jobdata_aggregate_result_values(results)

        ordinal, name = suitename.split('_', 1)
        t = {
            'name': suitename,
            'ordinal': int(ordinal),
            'result': result,
            'results': results,
        }
        t['url'] = '/results/{}/{}'.format(data['id'], suitename)
        suites[name] = t

    testcases = {}
    testgroups = (action['test'] for action in data['definition']['actions'] if 'test' in action)
    for testgroup in testgroups:
        for test in testgroup['definitions']:
            name = test['name']
            t = dict_subset(test, ('repository', 'path', 'revision'))
            if 'branch' in test: t['revision'] = test['branch']
            t['suite'] = name
            t['group'] = testgroup['name']
            # 'test-cases/foo.yaml' -> 'foo'
            t['testdefinition'] = t['path'].split('/')[-1].rsplit('.', 1)[0]

            if isinstance(t['repository'], dict):
                # skip inline test definitions
                continue

            action, extra = actions.get((t['repository'], t['path']), (None, None))
            t['commit_id'] = extra['commit'] if extra else None
            t['id'] = action['id'] if action else None

            suite = suites.get(name, {
                'name': name,
                'result': 'incomplete',
                'url': '/results/{}'.format(data['id']),
                })
            t.update(suite)

            testcases[name] = t

    return {'testcases': list(testcases.values()) }

def lava_jobdata_extract(data):
    data = lava_jobdata_fixup(data)
    jobdata = dict_subset(data, (
        'actual_device_id',
        'description',
        'end_time',
        'failure_comment',
        'id',
        'metadata',
        'start_time',
        'submit_time',
        'token',
        ))
    status_info = dict_subset(data, (
        'health_string',
        'state_string',
        'status_string'
        ))
    jobdata.update({ k: v.lower() for k, v in status_info.items() })
    if 'results' in data:
        jobdata.update(lava_jobdata_extract_results(data))
    if 'definition' in data:
        jobdata.update(lava_jobdata_extract_definition(data))
    return jobdata


class TestResult(namedtuple(
        'TestResult', [
            'id', 'metadata', 'name', 'result', 'suite', 'url',
            'commit_id', 'path', 'repository', 'revision', 'testdefinition',
        ])):
    def __new__(cls, data=None, **kwargs):
        if data is None:
            data = {}
        data.update(kwargs)
        data['id'] = int(data['id']) if data['id'] else None
        _, _, data['suite'] = data['suite'].rpartition('_')
        data = {k: data[k] for k in cls._fields}
        self = super().__new__(cls, **data)
        return self

    def __hash__(self):
        return hash(self.id)

class TestJob(object):
    def __init__(self, rawdata):
        self.data = {}
        self.metadata = {}
        self.results = {}
        self.id = int(rawdata['id'])
        self.description = rawdata.get('description')
        self.update(rawdata)

    def __hash__(self):
        return hash(self.id)

    def update(self, rawdata):
        assert self.id == int(rawdata['id'])
        self.description = rawdata.get('description')
        data = lava_jobdata_extract(rawdata)
        self.data.update(data)
        self.status = data['status_string']
        self.metadata.update(data.get('metadata', {}))
        self.parse_results(data)

    def parse_results(self, data):
        for r in data.get('testcases', []):
            result = TestResult(r, metadata=self.metadata)
            testdefinition = result.testdefinition
            self.results.setdefault(testdefinition, set()).add(result)

    @property
    def source_ref(self):
        source = self.metadata.get('source')
        if not isinstance(source, dict):
            source = {}
        ref = source.get('ref')
        if not ref:
            ref = self.metadata['image.osname'] + '/' + self.metadata['image.release']
        return ref

    @property
    def is_wip(self):
        wip = self.metadata.get('source.wip')
        if wip is None:
            wip = self.source_ref.startswith('wip')
        return wip

def main():
    parser = argparse.ArgumentParser(description='Process LAVA job results data.')
    parser.add_argument('inputfile', metavar='FILE', help='Read from FILE or stdin if "-" is used')
    args = parser.parse_args()
    inputfile = sys.stdin if args.inputfile == '-' else open(args.inputfile)
    data = json.load(inputfile)
    jobdata = lava_jobdata_extract(data)

    print(json.dumps(jobdata, sort_keys=True, indent=4))

if __name__ == "__main__":
    main()
