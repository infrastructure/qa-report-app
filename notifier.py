###################################################################################
# LAVA CI callback webservice
#
# Notifier module
#
# Copyright (C) 2018 Collabora Ltd
# Andrej Shadura <andrew.shadura@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from config import CommonNotificationTemplateArgs, NotificationsConfig, config
from itertools import groupby, tee
from functools import reduce
from typing import Optional, Sequence
from taskmanager import TestTask
from reports import generate_md_report, generate_text_report, generate_html_report
import json
import logging
from urllib.request import Request, urlopen
from urllib.error import HTTPError

import smtplib
from email.message import EmailMessage

class Mattermost(object):

    @staticmethod
    def format(settings: NotificationsConfig.Chat,
               args: NotificationsConfig.Chat.TextTemplateArgs,
               channel: Optional[str]) -> str:
        message = {
            'text': settings.text_template.substitute(args),
            'username': settings.username,
        }
        if settings.icon_url:
            message['icon_url'] = settings.icon_url
        if channel is not None:
            message['channel'] = channel

        return json.dumps(message)

    @staticmethod
    def notify(common_args: CommonNotificationTemplateArgs, *,
               created_tasks: Sequence[TestTask],
               updated_tasks: Sequence[TestTask]) -> None:
        if (settings := config.instance.notifications.chat) is None:
            return
        args = NotificationsConfig.Chat.TextTemplateArgs(
            report=generate_md_report(created_tasks, updated_tasks),
            common=common_args,
        )
        if settings.to:
            for chat in settings.to:
                Mattermost.submit(settings, args, chat)
        else:
            Mattermost.submit(settings, args)

    @staticmethod
    def submit(settings: NotificationsConfig.Chat,
               args: NotificationsConfig.Chat.TextTemplateArgs,
               channel: Optional[str] = None) -> None:
        json = Mattermost.format(settings, args, channel)
        r = Request(settings.url)
        r.add_header('Content-Type', 'application/json')
        try:
            urlopen(r, json.encode('UTF-8'))
        except HTTPError as e:
            logging.error('Chat request failed: %s', e)

class Email(object):
    @staticmethod
    def notify(common_args: CommonNotificationTemplateArgs, *,
               created_tasks: Sequence[TestTask],
               updated_tasks: Sequence[TestTask]) -> None:
        if (settings := config.instance.notifications.email) is None:
            return
        args = NotificationsConfig.Email.SubjectTemplateArgs(common_args)
        html = generate_html_report(created_tasks, updated_tasks)
        text = generate_text_report(created_tasks, updated_tasks)
        Email.submit(settings, args, html=html, text=text)
        pass

    @staticmethod
    def submit(settings: NotificationsConfig.Email,
               args: NotificationsConfig.Email.SubjectTemplateArgs,
               *,
               html: str,
               text: str) -> None:
        msg = EmailMessage()
        msg['Subject'] = settings.subject_template.substitute(args)
        msg['From'] = settings.from_
        msg['To'] = ', '.join(settings.to)
        msg.set_content(text)
        msg.add_alternative(html, subtype='html')
        print(msg)
        try:
            with smtplib.SMTP(host=settings.server, port=settings.port) as s:
                s.ehlo_or_helo_if_needed()
                try:
                    s.starttls()
                    s.ehlo()
                except (smtplib.SMTPNotSupportedError, RuntimeError):
                    logging.debug('Not using TLS')
                    pass
                if settings.user and settings.password:
                    s.login(settings.user, settings.password)
                s.send_message(msg)
        except smtplib.SMTPException as e:
            logging.error('Error sending email: %s', e)

def notify(tasks: Sequence[TestTask]) -> None:
    if not tasks:
        return
    created_tasks = sorted([t for t in tasks if t.new], key=lambda t: t.task_id)
    updated_tasks = sorted([t for t in tasks if not t.new], key=lambda t: t.task_id)
    created_arches = list({x['arch'] for x in reduce(lambda a, b: a + b.image_types, created_tasks, [])})
    updated_arches = list({x['arch'] for x in reduce(lambda a, b: a + b.image_types, updated_tasks, [])})
    common_args = CommonNotificationTemplateArgs(
        created_arches=', '.join(created_arches),
        updated_arches=', '.join(updated_arches),
        created_tasks_number=len(created_tasks),
        updated_tasks_number=len(updated_tasks),
        image_release=tasks[0].image_release,
        image_version=tasks[0].image_version,
    )
    Mattermost.notify(common_args,
                      created_tasks=created_tasks,
                      updated_tasks=updated_tasks)
    Email.notify(common_args,
                 created_tasks=created_tasks,
                 updated_tasks=updated_tasks)
