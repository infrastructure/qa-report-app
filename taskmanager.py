###################################################################################
# LAVA CI callback webservice
#
# Phab task manager module
#
# Copyright (C) 2018 Collabora Ltd
# Andrej Shadura <andrew.shadura@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import argparse
import gitlab
import json
import logging
import phabricator
import re
import textwrap
import yaml

from analyser import find_failures
from collections import OrderedDict
from config import AppConfig, BugTrackingConfig, config
from data.testcases_table import TestCasesTable
from functools import partial
from itertools import groupby
from phabricator import Phabricator, APIError
from pprint import PrettyPrinter
from testobjects import TestJob
from tests.utils import open_relative
from urllib.parse import urlparse

pprint = partial(lambda pp, *args: logging.debug(pp.pformat(*args)), PrettyPrinter(indent=2))

phab_api = None
gitlab_api = None

def connect_to_phab():
    global phab_api
    phabricator.ARCRC = {
        'hosts': {
            # The trailing slash is important! Due to the way python-phabricator
            # concatenates the URLs, missing a trailing slash will result in
            # none being present between the host URL and API method.
            f'{config.instance.phabricator_url}/api/': {
                'token': config.instance.phabricator_token,
            }
        }
    }

    phab_api = Phabricator()
    try:
        phab_api.update_interfaces()
        logging.info("Will talk to Phabricator as %s" % phab_api.user.whoami().userName)
    except Exception as e:
        logging.error("Phab API connection failed: %s", e)

def connect_to_gitlab():
    global gitlab_api

    try:
        gitlab_api = gitlab.Gitlab(config.instance.gitlab_url,
                                   private_token=config.instance.qa_report_gitlab_token)
    except Exception as e:
        logging.error("Gitlab API connection failed: %s", e)

def is_bug_tracking_available():
    return any([phab_api, gitlab_api])

def fetch_tasks_by_test(testdefinition):
    if phab_api:
        tasks = [x for x in
            phab_api.maniphest.search(queryKey='open', constraints={'projects': [config.instance.phabricator_tasks.filter_tag]}).data
            if x['fields']['custom.apertis:lava:suite'] == testdefinition
        ]

        return tasks
    elif gitlab_api:
        project = gitlab_api.projects.get(config.instance.gitlab_project)
        issues = project.issues.list(state='opened', label=[config.instance.gitlab_issues.filter_label], all=True)
        issues_filtered = []
        for i in issues:
            r = re.search(r'apertis:lava:suite: (.*)', i.description)
            if r and r[1] == testdefinition:
                issues_filtered.append(i)

        return issues_filtered

def get_task_title(task_id):
    if phab_api:
        data = phab_api.maniphest.search(constraints={'ids': [task_id]}).data
        title = data['fields']['name'].replace(']', '\\]')
        return title
    elif gitlab_api:
        project = gitlab_api.projects.get(config.instance.gitlab_project)
        issue = project.issues.get(task_id)
        return issue.title

def get_task_url(task_id):
    if phab_api:
        return "https://%s/T%d" % (urlparse(phab_api.host).netloc, task_id)
    elif gitlab_api:
        return "https://%s/%s/-/issues/%d" % (urlparse(config.instance.gitlab_url).netloc, config.instance.gitlab_project, task_id)

def phid_lookup(name):
    # the name cannot contain colons
    name = name.replace(':', '_')
    r = phab_api.phid.lookup(names=[name])
    if len(r) and name in r:
        return r[name]['phid']
    else:
        return None

def get_task_id(task):
    if phab_api:
        return task['id']
    elif gitlab_api:
        return task.iid

def maniphest_edit(task, transactions):
    if 'projects.add' in transactions:
        transactions['projects.add'] = [phid for phid in [phid_lookup('#' + slug) for slug in transactions['projects.add']] if phid]
    if 'space' in transactions:
        transactions['space'] = phid_lookup(transactions['space'])
    tr = [{'type': t, 'value': v} for t, v in transactions.items()]
    try:
        r = phab_api.maniphest.edit(objectIdentifier=task, transactions=tr)
        return r['object']['id']
    except APIError as e:
        logging.error("Phab API request failed for task %d: %s", task, e)
        pprint(tr)

def update_task(task_id, image_release, image_types, comment):
    if phab_api:
        transactions = {}
        projects = [ image_release ]
        for image_type in image_types:
            projects.append('architecture:' + image_type['arch'])
            projects.append('board:' + image_type['board'])
        transactions['projects.add'] = projects
        transactions['comment'] = comment
        maniphest_edit(task_id, transactions)
    elif gitlab_api:
        project = gitlab_api.projects.get(config.instance.gitlab_project)
        issue = project.issues.get(task_id)
        labels = [ 'release:' + image_release ]
        for image_type in image_types:
            labels.append('architecture:' + image_type['arch'])
            labels.append('board:' + image_type['board'])
        for label in labels:
            if label not in issue.labels:
                issue.labels.append(label)
        issue.notes.create({'body': comment})
        issue.save()

def create_task(title, description, image_release, image_types, testdefinition):
    if phab_api:
        phab_config = config.instance.phabricator_tasks

        transactions = {}
        transactions['title'] = title
        transactions['description'] = description
        projects = [phab_config.filter_tag, image_release]
        projects += phab_config.tags
        for image_type in image_types:
            projects.append('architecture:' + image_type['arch'])
            projects.append('board:' + image_type['board'])
        transactions['projects.add'] = projects
        transactions['space'] = phab_config.space
        transactions['custom.apertis:lava:suite'] = testdefinition
        return maniphest_edit(None, transactions)
    elif gitlab_api:
        gitlab_issues_config = config.instance.gitlab_issues

        project = gitlab_api.projects.get(config.instance.gitlab_project)
        issue = project.issues.create({'title': title,
                            'description': description})

        labels = [gitlab_issues_config.filter_label, 'release:' + image_release]
        labels += gitlab_issues_config.labels
        for image_type in image_types:
            labels.append('architecture:' + image_type['arch'])
            labels.append('board:' + image_type['board'])
        issue.labels = labels
        issue.save()

        return issue.iid

class TestTask(object):
    testcases = []
    task_id = None
    task_title = None
    task_url = None
    new = False
    test_priority = 'needs triage'

    def __init__(self, testcases, image_release, image_version):
        self.testcases = testcases
        self.testdefinition = testcases[0].testdefinition
        self.image_release = image_release
        self.image_version = image_version
        self.image_types = [{'arch': t.metadata['image.arch'],
                             'type': t.metadata['image.type'],
                             'board': t.metadata['image.board']} for t in self.testcases]

    def generate_comment(self):
        args = AppConfig.CommentTemplateArgs(
            case=self.testdefinition,
            app_url = config.instance.app_url,
            url_prefix = config.instance.url_prefix,
            image_release=self.image_release,
            image_releases=[self.image_release], # for compatibility
            image_version=self.image_version,
            image_types=', '.join(set('{arch} {type} {board}'.format(**t) for t in self.image_types)),
            logs='\n'.join(['* {log_url} on `{device}` with [{image_name}]({image_url})'.format(
                log_url = config.instance.lava_url + t.url,
                device = t.metadata['actual_device_id'],
                image_name = t.metadata['image.name'],
                image_url =  t.metadata['image.url']) for t in self.testcases])
        )
        return config.instance.comment_template.substitute(args)

    def generate_description(self):
        args = AppConfig.TaskTemplateArgs(
            test_case_name = self.testdefinition,
            app_url = config.instance.app_url,
            url_prefix = config.instance.url_prefix,
            image_release = self.image_release,
            image_version = self.image_version,
            test_priority = self.test_priority,
            image_versions = '\n'.join(['* [{image_name}]({image_url})'.format(
                image_name = t.metadata['image.name'],
                image_url = t.metadata['image.url'],
            ) for t in self.testcases]),
            logs='\n'.join(['* {log_url} on `{device}` with [{image_name}]({image_url})'.format(
                log_url = config.instance.lava_url + t.url,
                device = t.metadata['actual_device_id'],
                image_name = t.metadata['image.name'],
                image_url =  t.metadata['image.url']) for t in self.testcases])
        )
        return config.instance.task_template.substitute(args)

    def create_task(self):
        self.new = True
        self.task_title = '{testdefinition}: test failed'.format(
                testdefinition=self.testdefinition,
                )
        description = self.generate_description()
        self.task_id = create_task(self.task_title, description, self.image_release, self.image_types, self.testdefinition)
        self.task_url = get_task_url(self.task_id)
        logging.debug('created task %d', self.task_id)

    def update_task(self):
        self.new = False
        self.task_title = get_task_title(self.task_id)
        self.task_url = get_task_url(self.task_id)
        update_task(self.task_id, self.image_release, self.image_types, self.generate_comment())
        logging.debug('updated task %d', self.task_id)

    def process_task(self, testcases_table):
        tasks = fetch_tasks_by_test(self.testdefinition)
        is_task_per_release = False
        try:
            tc_file_data = testcases_table.get_tc_data(self.image_release, self.testdefinition + ".yaml")
        except KeyError:
            logging.warning(f'Unable to find test case for {self.image_release} {self.testdefinition}')
        else:
            data = yaml.safe_load(tc_file_data)
            if 'task-per-release' in data['metadata'].keys():
                task_per_release = data['metadata']['task-per-release']
            else:
                task_per_release = None

            if task_per_release == 'enable':
                is_task_per_release = True

            self.test_priority = data['metadata']['priority']

        found = False
        for task in tasks:
            task_id = get_task_id(task)
            if not is_task_per_release:
                found = True
                self.task_id = task_id
                self.update_task()
                break

            # FIXME: add support for 'task-per-release' when using phab as bugtracker
            if gitlab_api:
                project = gitlab_api.projects.get(config.instance.gitlab_project)
                issue = project.issues.get(task_id)
                release_label = 'release:' + self.image_release
                if release_label in issue.labels:
                    found = True
                    self.task_id = task_id
                    self.update_task()
                    break

        if not found:
            self.create_task()

def map_testcases_to_tasks(testcases, image_releases, image_version):
    for testdefinition, tests in testcases:
        for case, results in groupby(tests, lambda t: t.testdefinition):
            yield TestTask(list(results), image_releases, image_version)

from notifier import notify

def process_results(jobs, image_release, image_version, testcases_table):
    # In developement mode we usually don't use bug tracking
    if not is_bug_tracking_available():
        return
    # Copy the actual_device_id to metadata so it can be accessed in TestTask
    for job in jobs:
        job.metadata['actual_device_id'] = job.data['actual_device_id']

    failed_testcases = find_failures(jobs)
    tasks = list(map_testcases_to_tasks(failed_testcases, image_release, image_version))
    for t in tasks:
        t.process_task(testcases_table)

    created = len([t for t in tasks if t.new])
    updated = len(tasks) - created
    logging.info("updated %d tasks, created %d new tasks", updated, created)
    try:
        notify(tasks)
    except Exception as e:
        logging.info("unexpected error while posting notifications: %s", e)

def process_test_job(job_id, release, version, testcases_table):
    with open_relative(f'./test_data/job_data_{job_id}') as f:
        job_data = json.load(f)

        job = TestJob(job_data)

        process_results([job], release, version, testcases_table)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Test report application")
    parser.add_argument('-c', '--config', metavar='CONFIG',
                         help="configuration file")
    parser.add_argument('-j', '--job-id', help="job id", required=True)
    parser.add_argument('-r', '--release', help="image release", required=True)
    parser.add_argument('-i', '--version', help="image version", required=True)
    parser.add_argument('action', help="action to execute")

    args = parser.parse_args()

    if args.config:
        config.load_config_from_file(args.config)
    else:
        config.load_config_from_env_only()

    # Initialize after the config is loaded, so it can read the test case
    # repository configuration.
    testcases_table = TestCasesTable()
    testcases_table.initialize()

    bug_tracking = config.instance.bug_tracking
    if bug_tracking == BugTrackingConfig.PHABRICATOR:
        connect_to_phab()
    elif bug_tracking == BugTrackingConfig.GITLAB:
        connect_to_gitlab()
    else:
        logging.warn('No bug tracking configured')

    if args.action == 'test':
        process_test_job(args.job_id, args.release, args.version, testcases_table)
    else:
        print('Invalid action')
