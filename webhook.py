#!/usr/bin/env python3
###################################################################################
# Test reports web application.
#
# Copyright (C) 2019 Collabora Ltd
# Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import atexit
import argparse
import datetime
import hupper
import json
import logging
import os
import pathlib
import waitress

from apscheduler.schedulers.background import BackgroundScheduler
from atc_renderer.renderer import generate_test_case_page, \
    generate_index_page
from authlib.integrations.flask_client import OAuth
from collections import defaultdict
from config import AuthGroupPermission, BugTrackingConfig, config
from data.testcases_table import TestCasesTable
from flask import abort, Flask, request, redirect, session, url_for
from flask_healthz import healthz, HealthError
from forms import SelectImageForm, SubmitReportForm
from http import HTTPStatus
from models import db
from pages import generate_index, generate_report, \
    generate_select_image, generate_submit_report, \
    generate_internal_server_error, generate_forbidden
from save import save_job, save_manual_job, update_tags
from taskmanager import connect_to_phab, connect_to_gitlab, process_results
from testobjects import TestJob
from urllib.parse import urlparse, unquote_to_bytes


app = Flask(__name__)

# Queue of Jobs.
pending = defaultdict(set)
finished = defaultdict(set)
jobs = dict()

# This object contains the main test cases table with methods to
# keep it updated directly from the git repository.
#
# An initial tc table is created at object initialization and later will
# be updated from the background.
testcases_table = TestCasesTable()
table_update_interval = 900

authentication_disabled = False
local_username = "Local session"

oauth = OAuth(app)


def readiness():
    try:
        db.session.execute('SELECT 1')
    except Exception:
        logging.exception('Failed to execute basic query')
        raise HealthError('Failed to verify database connection')


app.config['HEALTHZ'] = {
    'live': lambda: None,
    'ready': readiness,
}

app.register_blueprint(healthz, url_prefix='/healthz')


def clear_session():
    session.pop('token', None)
    session.pop('username', None)
    session.pop('can-login', None)
    session.pop('can-submit-reports', None)
    session.pop('can-tag-images', None)

@app.route('/login')
def login():
    if authentication_disabled:
        return redirect(url_for('index'))

    logging.info('Attempting to login, next page is %s', request.args.get('next'))
    clear_session()
    redirect_uri = url_for('openid_callback', _external=True, next=request.args.get('next'))
    return oauth.openid.authorize_redirect(redirect_uri)

def user_is_member_of(user_groups, auth_groups):
    if authentication_disabled or not auth_groups:
        return True

    if not user_groups:
        return False

    allowed_auth_groups = {auth_group.name for auth_group in auth_groups}
    return bool(set(user_groups) & allowed_auth_groups)

def user_has_permission(perm, user_groups, auth_groups):
    if authentication_disabled or not auth_groups:
        return True

    if not user_groups:
        return False

    for group_name in user_groups:
        for auth_group in auth_groups:
            if auth_group.name == group_name and auth_group.has_permission(perm):
                return True
    return False

# Check that the user is logged in and return the username and user groups as tuple if it is.
# Return (None, None) otherwise.
def validate_user(token):
    if authentication_disabled:
        return local_username, None

    # Get the user/groups from the token if required info is provided
    user_info = token['userinfo']
    username = user_info.get('preferred_username')
    user_groups = user_info.get('groups')
    if username is None or user_groups is None:
        # Fallback to get the user/groups from 'userinfo' endpoint
        user_info = oauth.openid.userinfo(token=token)
        username = username or user_info['preferred_username']
        user_groups = user_groups or user_info['groups']
    return username, user_groups

def update_user_perms(groups):
    if authentication_disabled:
        session['can-login'] = True
        session['can-submit-reports'] = True
        session['can-tag-images'] = True
        return

    logging.info('username=%s: Checking user permissions, found groups %s',
                 session.get('username'), groups)
    session['can-login'] = user_is_member_of(groups, config.instance.auth_groups)
    session['can-submit-reports'] = user_has_permission(AuthGroupPermission.SUBMIT_REPORTS,
                                                        groups, config.instance.auth_groups)
    session['can-tag-images'] = user_has_permission(AuthGroupPermission.TAG_IMAGES,
                                                    groups, config.instance.auth_groups)

# Callback for openid connect
@app.route("/openid_callback")
def openid_callback():
    if authentication_disabled:
        return redirect(url_for('index'))

    next_page = request.args.get('next') or url_for('index')
    logging.info('Validating login, next page is %s', next_page)
    try:
        token = oauth.openid.authorize_access_token()

        username, user_groups = validate_user(token)

        # authlib saves the decoded JWT's ID token as token['userinfo'] which may
        # contain many fields/values, so let's strip it down to avoid "too large"
        # issues when saving the token (required to further
        # validate user is logged in on page requests) in the session.
        session_token = token.copy()
        session_token.pop('userinfo', None)
        session_token.pop('id_token', None)
        if config.instance.openid.token_refresh_interval > 0:
            now = int(datetime.datetime.now().timestamp())
            session_token['expires_at'] = now + config.instance.openid.token_refresh_interval
        session['token'] = session_token
        session['username'] = username
        update_user_perms(user_groups)
    except Exception as e:
        clear_session()
        logging.exception(e)
        abort(HTTPStatus.INTERNAL_SERVER_ERROR, description='unable to retrieve user info')

    return redirect(next_page)

@app.before_request
def check_login_status():
    if request.endpoint in ['login', 'openid_callback', 'post_results', 'static', 'healthz.check']:
        return None

    if authentication_disabled:
        session['username'] = local_username
        update_user_perms(None)
        return None

    token = session.get('token')
    if token is not None:
        expires_at = int(token.get('expires_at', 0))
        now = int(datetime.datetime.now().timestamp())

        if 0 < expires_at < now:
            logging.info('Token refresh timeout expired, refreshing token, next page is %s', request.path)
            clear_session()
            return redirect(url_for('login', next=request.path))

        try:
            # Fetch latest user info from 'userinfo' endpoint to verify token
            # is still valid
            user_info = oauth.openid.userinfo(token=token)
            # Update permissions from the latest info if provided by
            # the 'userinfo' endpoint
            if 'groups' in user_info:
                update_user_perms(user_info['groups'])
        except Exception as e:
            logging.exception(e)
            clear_session()

    username = session.get('username')
    if username is not None:
        if not session.get('can-login'):
            clear_session()
            logging.error("username=%s: Failed to access %s, not part of allowed auth-groups", username, request.path)
            abort(HTTPStatus.FORBIDDEN, description='user is not part of allowed authentication groups')
    elif config.instance.openid.always_require_login:
        return redirect(url_for('login', next=request.path))

    return None

@app.route('/report/<image_release>/<image_version>')
@app.route('/report/<image_release>/<image_version>/<image_deployment>',
           methods=['GET', 'POST'])
@app.route('/report/<image_release>/<image_version>/<image_deployment>/<show>')
def get_report(image_release, image_version,
               image_deployment='apt', show='run'):

    username = session.get('username')

    user_can_tag_images = session.get('can-tag-images')

    if request.method == 'POST' and username is not None:
        if not user_can_tag_images:
            logging.error("username=%s: Failed to tag image, not part of allowed auth-groups", username)
            abort(HTTPStatus.FORBIDDEN, description='user is not part of allowed authentication groups')

        is_checked, tag, tag_type = request.form.get('is_checked'), request.form.get('tag'), request.form.get('tag_type')
        logging.info("Receiving tag: '%s' (type: %s, checked: %s)", tag, tag_type, is_checked)
        update_tags(image_release, image_version,
                    tag, is_checked, tag_type, db)
        return '', HTTPStatus.OK

    try:
        return generate_report(image_release, image_version,
                               image_deployment.lower(), show.lower(),
                               testcases_table.get(), username, user_can_tag_images)
    except:
        logging.exception(f'Failed to generate report at URL {request.path}:')
        return 'Report error', HTTPStatus.NOT_FOUND

@app.route('/', methods=['GET'])
@app.route('/all.html', methods=['GET'], endpoint='index_all')
def index():
    username = session.get('username')
    show_all = request.path == '/all.html'

    user_can_submit_reports = session.get('can-submit-reports')

    try:
        return generate_index(testcases_table, username, user_can_submit_reports, show_all=show_all)
    except:
        logging.exception('Failed generate index:')
        return 'Reports list error', HTTPStatus.NOT_FOUND

@app.route('/testcases/<release>')
def testcases_release_index(release):
    index_files_data = testcases_table.get_index_files_data()
    return generate_index_page(index_files_data,
                               release,
                               static_url = url_for('static', filename=''))

@app.route('/testcases/<release>/<testcase>.html')
def testcases_definition(release, testcase):
    tc_file_data = testcases_table.get_tc_data(release, testcase + ".yaml")
    tc_images_data = testcases_table.get_tc_images(release)
    if not tc_file_data or not tc_images_data:
        abort(HTTPStatus.NOT_FOUND, description='Unable to retrieve requested resource')
    return generate_test_case_page(tc_file_data,
                                   tc_images_data,
                                   static_url = url_for('static', filename=''))

@app.route('/<release>/<testcase>.html')
def release_testcases(release, testcase):
    return redirect(url_for('testcases_definition',
                    release=release,
                    testcase=testcase),
                    code=308)

@app.route('/latest/<testcase>.html')
def latest_testcases(testcase):
    default_branch = testcases_table.get_default_branch()
    return redirect(url_for('testcases_definition',
                    release=default_branch,
                    testcase=testcase),
                    code=307)

@app.route('/<testcase>.html')
def testcases(testcase):
    default_branch = testcases_table.get_default_branch()
    return redirect(url_for('testcases_definition',
                    release=default_branch,
                    testcase=testcase),
                    code=307)

@app.route('/select_image', methods=['GET', 'POST'])
def select_image():
    username = session.get('username')
    if username is None:
        return redirect(url_for('login', next=request.path))

    user_can_submit_reports = session.get('can-submit-reports')
    if not user_can_submit_reports:
        logging.error("username=%s: Failed to submit report, not part of allowed auth-groups", username)
        abort(HTTPStatus.FORBIDDEN, description='user is not part of allowed authentication groups')

    form = SelectImageForm(config.instance.images)
    if form.validate_on_submit():
        return redirect(url_for('submit_report',
                                image_release=request.form['image_release'],
                                image_version=request.form['image_version'],
                                image_type=request.form['image_type'],
                                image_deployment=request.form['image_deployment']))

    return generate_select_image(username, form)

@app.route('/submit_report/<image_release>/<image_version>/'
           '<image_type>/<image_deployment>',
           methods=['GET', 'POST'])
def submit_report(image_release, image_version, image_type, image_deployment):
    username = session.get('username')
    if username is None:
        return redirect(url_for('login', next=request.path))

    user_can_submit_reports = session.get('can-submit-reports')
    if not user_can_submit_reports:
        logging.error("username=%s: Failed to submit report, not part of allowed auth-groups", username)
        abort(HTTPStatus.FORBIDDEN, description='user is not part of allowed authentication groups')

    form = SubmitReportForm()
    if form.validate_on_submit():
        # Convert the ImmutableDict from request.form into a plain dictionary.
        manual_tests_results = request.form.to_dict()
        # Remove csrf_token received by the form.
        manual_tests_results.pop('csrf_token')

        save_manual_job(username, manual_tests_results, db,
                        image_release, image_version, image_type, image_deployment)
        return redirect(url_for('index'))

    # Generate a submit report page with only manual test cases for the specified
    # image type and deployment.
    return \
        generate_submit_report(username, form,
                               image_release, image_version,
                               image_deployment, image_type,
                               testcases_table.get((image_deployment, image_type, image_release)))

@app.route('/', methods=['POST'])
@app.route('/lava/post', methods=['POST'])
def post_results():
    data = request.get_data(as_text=True)
    logdata('webhook', data)
    try:
        job_data = json.loads(data)
    except ValueError as e:
        return ('JSON decoding error %s' % e), HTTPStatus.BAD_REQUEST

    # Check job data is received with correct credentials.
    tokens = config.instance.lava_callback_tokens
    if tokens and not ('Authorization' in request.headers and request.headers['Authorization'] in tokens):
        return 'Token error, got "{}"'.format(request.headers.get('Authorization')), HTTPStatus.FORBIDDEN

    # Check for minimal requirements to process job.
    if not 'id' in job_data:
        return 'Missing job ID', HTTPStatus.BAD_REQUEST
    elif not 'status_string' in job_data:
        return 'Missing job status', HTTPStatus.BAD_REQUEST

    # Process job according to status.
    elif job_data['status_string'] == 'submitted':
        return register_job(job_data)
    elif job_data['status_string'] in ['complete', 'incomplete', 'canceled']:
        return process_job(job_data)
    # Unknown data...
    else:
        return 'Unknown status, got "{}"'.format(job_data.get('status_string')), HTTPStatus.BAD_REQUEST

@app.errorhandler(HTTPStatus.FORBIDDEN)
def forbidden(error):
    return generate_forbidden(error)

@app.errorhandler(HTTPStatus.INTERNAL_SERVER_ERROR)
def internal_server_error(error):
    return generate_internal_server_error(error)

def register_job(job_data):
    """
    This routine is invoked on POST for registering submitted jobs.
    """

    if not 'metadata' in job_data:
        return 'Missing job metadata', HTTPStatus.BAD_REQUEST

    job_id = int(job_data['id'])
    metadata = job_data['metadata']
    if not 'image.release' in metadata or not 'image.version' in metadata:
        return 'Missing image metadata', HTTPStatus.BAD_REQUEST

    image_release = metadata['image.release']
    image_version = metadata['image.version']
    build_tuple = (image_release, image_version)

    if job_id not in jobs:
        pending[build_tuple].add(job_id)
        jobs[job_id] = TestJob(job_data)
        logging.debug('added job %s as pending for image %s', job_id,
                      build_tuple)
        logging.debug('pending: %r', dict(pending))
        logging.debug('complete: %r', dict(finished))
        logging.debug('jobs: %r', jobs.keys())
        logging.info('Registering job: %d', job_id)

        msg = 'Added job %d as pending for image %s' % (job_id, build_tuple)
        return msg, HTTPStatus.OK
    else:
        msg = 'Job %d already known for image %s' % (job_id, build_tuple)
        return msg, HTTPStatus.ALREADY_REPORTED

def process_job(job_data):
    """
    This routine is invoked on POST for processing finalized jobs.
    """

    job_id = int(job_data['id'])

    # Update TestJob once final job data is received, or simply add it if not pre-registered
    jobs.setdefault(job_id, TestJob(job_data)).update(job_data)

    # Remove job from pending queue and add to finished list.
    image_release = jobs[job_id].metadata['image.release']
    image_version = jobs[job_id].metadata['image.version']
    build_tuple = (image_release, image_version)
    pending[build_tuple].discard(job_id)
    finished[build_tuple].add(job_id)

    # Save data into DB as soon as the job is received.
    save_job(jobs[job_id], db)
    logging.info('Job %d saved into the database', job_id)

    # Notify results once all jobs are available for an image version.
    if not len(pending[build_tuple]):
        logging.info('Processing jobs %r for %s', finished[build_tuple],
                     build_tuple)
        logging.info('Sending notifications for image version %s ...',
                     build_tuple)
        del pending[build_tuple]
        series_complete(build_tuple)

    msg = 'Marked job %d as finished with status %s' % \
          (job_id, jobs[job_id].status)
    return msg, HTTPStatus.OK

def series_complete(build_tuple):
    """Function to process all jobs for an image version"""
    filtered_jobs = [jobs[job_id] for job_id in finished[build_tuple]]
    # TODO: Notification needs to be adapted to use the new TestObject format.
    image_release, image_version = build_tuple
    process_results(filtered_jobs, image_release, image_version, testcases_table)
    del finished[build_tuple]

def update_testcases_table():
    logging.info('Updating testcases_table data')
    testcases_table.update()

def logdata(datatype, data):
    try:
        logdir = '/tmp/lavaphabbridge-logs'
        timestamp = datetime.datetime.now().timestamp()
        logfile = '{}/lavaphabbridge-{}-{}.log'.format(logdir, datatype, timestamp)
        os.makedirs(logdir, exist_ok=True)
        with open(logfile, 'w') as f:
            f.write(data)
    except:
        pass

def init_testcases_table_scheduler():
    """
    This function setups a scheduler background process to update the main
    testcases_table structure from the apertis-test-cases repository at intervals
    specified by `seconds`.
    """
    scheduler = BackgroundScheduler()
    scheduler.add_job(func=update_testcases_table,
                      trigger="interval",
                      seconds=table_update_interval)
    scheduler.start()
    atexit.register(lambda: scheduler.shutdown())

@app.after_request
def log_response(response):
    if response.status_code >= 400:
        logging.warning('Returning error: {} "{}"'.format(response.status_code, response.get_data(as_text=True).split('\n')[0]))
    return response

def initialize_database():
    app.config['SQLALCHEMY_DATABASE_URI'] = config.instance.db.uri
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)

def main():
    global authentication_disabled
    parser = argparse.ArgumentParser(description="Test report application")
    parser.add_argument('-c', '--config', metavar='CONFIG',
                         help="configuration file")
    parser.add_argument('--log-file', metavar='LOG_FILE',
                         help="log file")
    parser.add_argument('-d', '--debug', action='store_true', default=False,
                        help="print more debugging info")
    parser.add_argument('-p', '--port', type=int,
                        help="specify the callback URL")
    parser.add_argument('--skip-bug-tracking', action='store_true', default=False,
                        help="don't integrate with bug trackers")
    parser.add_argument('--disable-authentication', action='store_true', default=False,
                        help="local development session")
    parser.add_argument('--live-reload', action='store_true', default=False,
                        help="live reload server on source changes")
    args = parser.parse_args()

    if args.skip_bug_tracking:
        config.skip_bug_tracking_validation = True

    if args.config:
        config.load_config_from_file(args.config)
    else:
        config.load_config_from_env_only()

    if args.log_file is not None:
        config.instance.log_file = args.log_file
    if args.debug:
        config.instance.debug = True
    if args.port is not None:
        config.instance.port = args.port

    authentication_disabled = args.disable_authentication or config.instance.openid is None

    logging.basicConfig(filename=config.instance.log_file, filemode='w',
                        level=(logging.INFO if not config.instance.debug \
                               else logging.DEBUG),
                        format='%(asctime)s: %(levelname)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    app.debug = config.instance.debug

    initialize_database()

    if not args.skip_bug_tracking:
        bug_tracking = config.instance.bug_tracking
        if bug_tracking == BugTrackingConfig.PHABRICATOR:
            connect_to_phab()
        elif bug_tracking == BugTrackingConfig.GITLAB:
            connect_to_gitlab()

    # Initialize after the config is loaded, so it can read the test case
    # repository configuration.
    testcases_table.initialize()
    init_testcases_table_scheduler()

    app.secret_key = config.instance.flask_secret_key

    if not authentication_disabled:
        if config.instance.openid.manual:
            client_params = config.instance.openid.manual.dict(exclude_none=True)
        elif config.instance.openid.well_known_url:
            client_params = { "server_metadata_url": config.instance.openid.well_known_url }
        else:
            # should never happen given authentication_disabled is True if
            # config.instance.openid is unset
            assert False

        oauth.register(
            name='openid',
            client_id=config.instance.openid.client_id,
            client_secret=config.instance.openid.client_secret,
            **client_params,
            client_kwargs={
                'scope': 'openid email profile'
            }
        )

    if args.live_reload:
        hupper.start_reloader(pathlib.Path(__file__).stem + '.main')
        app.config['TEMPLATES_AUTO_RELOAD'] = True

    if (proxy_count := config.instance.proxy_count):
        trusted_proxy_kw = {
            'trusted_proxy': '*',
            'trusted_proxy_count': proxy_count,
            'trusted_proxy_headers': {'x-forwarded-for', 'x-forwarded-host', 'x-forwarded-proto'}
        }
    else:
        trusted_proxy_kw = {}

    app.config['APPLICATION_ROOT'] = config.instance.url_prefix

    waitress.serve(app, host='0.0.0.0', port=config.instance.port,
        url_prefix=config.instance.url_prefix,
        **trusted_proxy_kw)

if __name__ == '__main__':
    main()
