# Copyright © 2019 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import json
import os
import pygit2
import sys
import unittest

import flask_migrate
import lxml.etree
import phabricator
import pytest

from testing.postgresql import Postgresql

# Import modules from the current and parent directory in case pytest is
# invoked in this folder or from the project root
sys.path += ['.', '..']

import models
import save
from testutils import import_app_with_custom_db_instance, submit_test_data, submit_manual_test_data
from utils import open_relative

class FunctionalTestCase(unittest.TestCase):
    def setUp(self):
        self.postgresql = Postgresql()
        self.app, self.db = import_app_with_custom_db_instance(self.postgresql)
        migrate = flask_migrate.Migrate(self.app, self.db)
        with self.app.app_context():
            flask_migrate.upgrade()

        import taskmanager, webhook
        webhook.testcases_table.initialize()

    def tearDown(self):
        self.postgresql.stop()

    def test_index(self):
        client = self.app.test_client()
        response = client.get('/')
        assert response.status_code == 200
        assert response.mimetype == 'text/html'
        assert response.charset == 'utf-8'
        parser = lxml.etree.HTMLParser(recover=False)
        contents = lxml.etree.HTML(response.data.decode(response.charset))
        assert contents.xpath('head/title') != None
        assert contents.xpath('body') != None

    def get_default_branch(self):
        from data.testcases_table import test_cases_repo, get_default_branch
        repo = pygit2.Repository(test_cases_repo)
        return get_default_branch(repo)

    def test_release_testcase_redirect(self):
        client = self.app.test_client()
        response = client.get('/v2022/ade-commands.html')
        assert response.status_code == 308
        assert response.location == "/testcases/v2022/ade-commands.html"

    def test_latest_testcase_redirect(self):
        client = self.app.test_client()
        default_branch = self.get_default_branch()
        location = "/testcases/" + default_branch + "/ade-commands.html"
        response = client.get('/latest/ade-commands.html')
        assert response.status_code == 307
        assert response.location == location

    def test_testcase_redirect(self):
        client = self.app.test_client()
        default_branch = self.get_default_branch()
        location = "/testcases/" + default_branch + "/ade-commands.html"
        response = client.get('/ade-commands.html')
        assert response.status_code == 307
        assert response.location == location

    def test_lava_submit_single_job_succeeding(self):
        job_id = 1450933
        response = submit_test_data(self.app, job_id)
        assert response.status_code == 200
        with self.app.app_context():
            assert models.Job.query.count() == 1
            job = models.Job.query.filter_by(lava_job_id=job_id).one()
            assert job
            assert job.description == 'AppArmor common tests on 19.03 Minnowboard turbot using target OStree image 20190125.0'
            assert job.exec_type == 'automated'
            assert job.image_architecture == 'amd64'
            assert job.image_build == '20190125.0'
            assert job.image_deployment == 'ostree'
            assert job.image_platform == 'uefi'
            assert job.image_release == '19.03'
            assert job.image_type == 'target'
            assert job.image_url == 'https://images.apertis.org/daily/19.03/20190125.0/amd64/target/apertis_ostree_19.03-target-amd64-uefi_20190125.0.img.gz'
            assert job.lava_actual_device_id == 'minnowboard-turbot-E3826-cbg-0'
            assert job.lava_device_id == 'minnowboard-turbot-E3826'
            assert job.lava_health == 'complete'
            assert job.lava_job_id == 1450933
            assert job.lava_state == 'finished'
            assert job.lava_status == 'complete'
            assert job.visibility == 'public'
            print('job.testcases', job.testcases)
            assert len(job.testcases) == 9
            assert all(testcase.result == 'pass' for testcase in job.testcases)
            assert job.testcases[0].lava_url == '/results/1450933/0_sanity-check'
            assert job.testcases[0].path == 'test-cases/sanity-check.yaml'
            assert job.testcases[0].result == 'pass'

    def test_lava_submit_single_job_with_failures(self):
        phabricator_calls = []
        job_id = 1377334
        response = submit_test_data(self.app, job_id, phabricator_calls)
        assert response.status_code == 200
        print('phabricator_calls', phabricator_calls)
        edits = [call for call in phabricator_calls if call.name == 'maniphest.edit']
        edits.sort(key=lambda i: i.transaction_value('title'))
        assert len(edits) == 3
        assert edits[0].transaction_value('title') == 'frome: test failed'
        assert edits[0].transaction_value('custom.apertis:lava:suite') == 'frome'
        assert 'PHID-PROJ-fake-bug' in edits[0].transaction_value('projects.add')
        assert edits[1].transaction_value('title') == 'ribchester: test failed'
        assert edits[2].transaction_value('title') == 'traprain: test failed'
        with self.app.app_context():
            assert models.Job.query.count() == 1
            job = models.Job.query.filter_by(lava_job_id=job_id).one()
            assert job
            assert job.image_architecture == 'amd64'
            assert job.image_build == '20190925.0'
            assert job.image_deployment == 'apt'
            assert job.image_platform == 'sdk'
            assert job.image_release == 'v2019'
            assert job.image_type == 'sdk'
            assert job.image_url == 'https://images.apertis.org/release/v2019/v2019.7/amd64/sdk/apertis_v2019-sdk-amd64-sdk_v2019.7.img.gz'
            print('job.testcases', job.testcases)
            assert len(job.testcases) == 8
            assert len([testcase for testcase in job.testcases if testcase.result == 'fail']) == 3
            assert job.testcases[3].path == 'modules/frome.yaml'
            assert job.testcases[3].result == 'fail'
            assert job.testcases[6].path == 'modules/ribchester.yaml'
            assert job.testcases[6].result == 'fail'

    def test_lava_submit_job_from_secondary_branch(self):
        phabricator_calls = []
        job_id = 2431390
        response = submit_test_data(self.app, job_id, phabricator_calls)
        assert response.status_code == 200
        assert not [c for c in phabricator_calls if c.name == 'maniphest.edit']
        with self.app.app_context():
            assert models.Job.query.count() == 1
            job = models.Job.query.filter_by(lava_job_id=job_id).one()
            assert job
            assert job.image_url == 'https://images.apertis.org/test/wip-em-v2020-increase-timeout-sdk/daily/v2020/20200528.1414/armhf/minimal/apertis_ostree_v2020-minimal-armhf-uboot_20200528.1414.img.gz'

    def test_lava_submit_missing_end_time_workaround(self):
        job_id = 4122603
        response = submit_test_data(self.app, job_id)
        assert response.status_code == 200
        with self.app.app_context():
            assert models.Job.query.count() == 1
            job = models.Job.query.filter_by(lava_job_id=job_id).one()
            assert job

    def test_lava_submit_single_job_with_failures_using_branch(self):
        phabricator_calls = []
        job_id = 5377334
        response = submit_test_data(self.app, job_id, phabricator_calls)
        assert response.status_code == 200
        print('phabricator_calls', phabricator_calls)
        edits = [call for call in phabricator_calls if call.name == 'maniphest.edit']
        edits.sort(key=lambda i: i.transaction_value('title'))
        assert len(edits) == 3
        assert edits[0].transaction_value('title') == 'frome: test failed'
        assert edits[0].transaction_value('custom.apertis:lava:suite') == 'frome'
        assert 'PHID-PROJ-fake-bug' in edits[0].transaction_value('projects.add')
        assert edits[1].transaction_value('title') == 'ribchester: test failed'
        assert edits[2].transaction_value('title') == 'traprain: test failed'
        with self.app.app_context():
            assert models.Job.query.count() == 1
            job = models.Job.query.filter_by(lava_job_id=job_id).one()
            assert job
            assert job.image_architecture == 'amd64'
            assert job.image_build == '20190925.0'
            assert job.image_deployment == 'apt'
            assert job.image_platform == 'sdk'
            assert job.image_release == 'v2019'
            assert job.image_type == 'sdk'
            assert job.image_url == 'https://images.apertis.org/release/v2019/v2019.7/amd64/sdk/apertis_v2019-sdk-amd64-sdk_v2019.7.img.gz'
            print('job.testcases', job.testcases)
            assert len(job.testcases) == 8
            assert len([testcase for testcase in job.testcases if testcase.result == 'fail']) == 3
            assert job.testcases[3].path == 'modules/frome.yaml'
            assert job.testcases[3].result == 'fail'
            assert job.testcases[6].path == 'modules/ribchester.yaml'
            assert job.testcases[6].result == 'fail'

    def test_overview(self):
        job_ids = [
                1377334,
                1450933,
                1491564,
                1560987,
                1560988,
                1577786,
                1663435,
                2640299,
                ]
        for job_id in job_ids:
            submit_test_data(self.app, job_id)
        submit_manual_test_data(self.app, self.db)
        tag = 'Test release'
        with self.app.app_context():
            save.update_tags('v2022dev0', '20200920.2038', tag, 'true', 'release', self.db)
            save.update_tags('v2019dev0', '20190412.0', '', 'true', 'weekly', self.db)
            entries = models.reports_overview()
        tagged = [e for e in entries if e.tag_type is not None]
        released = [e for e in entries if e.tag_type == str(models.TagType.release)]
        weeklies = [e for e in entries if e.tag_type == str(models.TagType.weekly)]
        assert len(released) == 1
        assert released[0].tag == tag
        assert len(weeklies) == 1
        assert len(tagged) == 2
        for entry in entries:
            print("entry:", dict(entry))
            for r in entry.results.values():
                assert r['executed'] == r['automated']['total'] + r['manual']['total'], f"Partials by type do not add up for {entry.image_release}/{entry.image_build}"
                assert r['automated']['total'] == r['automated']['pass'] + r['automated']['skip'] + r['automated']['fail'] + r['automated']['incomplete'], f"Partials by result do not add up for {entry.image_release}/{entry.image_build}"
                assert r['manual']['total'] == r['manual']['pass'] + r['manual']['skip'] + r['manual']['fail'] + r['manual']['incomplete'], f"Partials by result do not add up for {entry.image_release}/{entry.image_build}"
