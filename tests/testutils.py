# Copyright © 2019 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import importlib
import json
import os

from dataclasses import dataclass

from unittest.mock import MagicMock, patch

from config import config
from utils import open_relative

import taskmanager

os.environ['PHABRICATOR_URL'] = 'http://localhost'
os.environ['PHABRICATOR_TOKEN'] = 'foo'

def import_app_with_custom_db_instance(postgresql, display=None):
    dsn = postgresql.dsn()

    os.environ['DB_USER'] = dsn['user']
    os.environ['DB_PASSWORD'] = 'foo'
    os.environ['DB_HOST'] = dsn['host']
    os.environ['DB_PORT'] = str(dsn['port'])
    os.environ['DB_NAME'] = dsn['database']

    config.load_config_from_env_only()

    config.instance.display = display

    import webhook
    importlib.reload(webhook)  # ensure the app gets re-created
    webhook.initialize_database()
    webhook.app.secret_key = "secret_key"
    webhook.authentication_disabled = True
    return webhook.app, webhook.db

@dataclass
class Call:
    name: str
    args: tuple
    kwargs: dict

    def transaction_value(self, transaction_type, **kwargs):
        return next((t['value'] for t in self.kwargs['transactions'] if t['type'] == transaction_type), **kwargs)

class FakePhabricator:
    @staticmethod
    def _phid_lookup(names):
        ret = {}
        for key in names:
            if key.startswith('#'):
                name = key.lstrip('#')
                ret.update({
                    key: {
                        "phid": f"PHID-PROJ-fake-{name}",
                        "uri": f"https://phabricator.apertis.org/tag/{name}/",
                        "typeName": "Project",
                        "type": "PROJ",
                        "name": name,
                        "fullName": name,
                        "status": "open",
                        }
                    })
        return ret

def _mock_phabricator_request(output_call_list):
    class Stub:
        def _request(self, **kwargs):
            call = Call('{}.{}'.format(self.method, self.endpoint), (), kwargs)
            print(call)
            if output_call_list is not None:
                output_call_list.append(call)
            if f'{self.method}.{self.endpoint}' == 'phid.lookup':
                return FakePhabricator._phid_lookup(**kwargs)
            return MagicMock()
    return Stub._request

def submit_test_data(app, job_id, captured_phabricator_calls=None):
    phabricator_request_mocker = _mock_phabricator_request(captured_phabricator_calls)
    with patch('phabricator.Resource._request', new=phabricator_request_mocker):
        client = app.test_client()
        taskmanager.connect_to_phab()
        with open_relative(f'./test_data/job_data_{job_id}') as f:
            data = json.load(f)
        return client.post('/lava/post', json=data)

def submit_manual_test_data(app, db):
    from save import save_manual_job
    from test_data.manual_job_data import job_data
    with app.app_context():
        save_manual_job('tester', job_data, db, 'v2021dev1',
                        '20200213.0', 'target-amd64-uefi-public', 'ostree')

def needle_fixup_get_screenshot():
    """When capturing screenshots, only the contents in the viewport are
       rendered. However, Needle insists in creating images which are as big
       as the actual element, which means that if the element is larger than
       the viewport the final image will contain black unrendered areas.
       This fixup limits the size of the image to the one of the viewport,
       trimming away any potential blank area."""
    import needle.driver
    orig_get_screenshot = needle.driver.NeedleWebElementMixin.get_screenshot
    def override_get_screenshot(self):
        image = orig_get_screenshot(self)
        width, height = image.size
        window_size = self.parent.get_window_size()
        width = min(width, window_size['width'])
        height = min(height, window_size['height'])
        image = image.crop((0, 0, width, height))
        return image
    needle.driver.NeedleWebElementMixin.orig_get_screenshot = orig_get_screenshot
    needle.driver.NeedleWebElementMixin.get_screenshot = override_get_screenshot

def webdriver_fixup_disable_transitions():
    """Disable transitions to avoid capturing contents midway during an
       animation, resulting in mismatched comparisons.
       For instance, the CSS from Bootstrap used for the "Login" button was
       transitioning the color on load, making tests succeed only sometimes.
    """
    import selenium.webdriver.remote.webdriver
    orig_get = selenium.webdriver.remote.webdriver.WebDriver.get
    def override_get(self, url):
        orig_get(self, url)
        disable_transitions = """
            styleEl = document.createElement('style');
            styleEl.textContent = '* { transition-property: none !important; }';
            document.head.appendChild(styleEl);
        """
        self.execute_script(disable_transitions)
    selenium.webdriver.remote.webdriver.WebDriver.orig_get = orig_get
    selenium.webdriver.remote.webdriver.WebDriver.get = override_get
