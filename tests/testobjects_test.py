# Copyright © 2019 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import json
import os
import sys

import pytest

# Import modules from the current and parent directory in case pytest is
# invoked in this folder or from the project root
sys.path += ['.', '..']

from utils import open_relative
from testobjects import TestJob

def test_load_1450933():
    with open_relative('./test_data/job_data_1450933') as f:
        rawdata = json.load(f)
    t = TestJob(rawdata)
    assert t.id == 1450933
    assert t.status == 'complete'
    assert t.description == 'AppArmor common tests on 19.03 Minnowboard turbot using target OStree image 20190125.0'
    assert t.metadata['image.version'] == '20190125.0'
    assert t.metadata['image.release'] == '19.03'
    assert t.metadata['image.type'] == 'target'
    assert t.metadata['image.arch'] == 'amd64'
    assert t.metadata['image.board'] == 'uefi'
    assert t.metadata['image.osname'] == 'apertis'
    assert t.metadata['image.flavour'] == 'ostree'
    assert t.metadata['image.name'] == 'apertis_ostree_19.03-target-amd64-uefi_20190125.0.img.gz'
    assert t.results['sanity-check']
    for r in t.results['sanity-check']:
        assert r.result == 'pass'
    for r in t.results['apparmor']:
        assert r.result in ('pass', 'skip')
    for r in t.results['apparmor-gstreamer1-0']:
        assert r.result == 'pass'
    for r in t.results['apparmor-tumbler']:
        assert r.result == 'pass'
    for r in t.results['apparmor-dbus']:
        assert r.result == 'pass'
    for r in t.results['apparmor-basic-profiles']:
        assert r.result == 'pass'
    for r in t.results['apparmor-pulseaudio']:
        assert r.result == 'pass'
    for r in t.results['apparmor-chaiwala-system']:
        assert r.result == 'pass'

def test_load_1377334():
    with open_relative('./test_data/job_data_1377334') as f:
        rawdata = json.load(f)
    t = TestJob(rawdata)
    assert t.id == 1377334
    assert t.status == 'complete'
    assert t.description == 'Modules tests on v2019 QEMU using SDK image 20190925.0'
    assert t.metadata['image.version'] == '20190925.0'
    assert t.metadata['image.release'] == 'v2019'
    assert t.metadata['image.type'] == 'sdk'
    assert t.metadata['image.arch'] == 'amd64'
    assert t.metadata['image.board'] == 'sdk'
    assert t.metadata['image.osname'] == 'apertis'
    assert t.metadata['image.flavour'] == ''
    assert t.metadata['image.name'] == 'apertis_v2019-sdk-amd64-sdk_v2019.7.img.gz'
    assert t.results['sanity-check']
    for r in t.results['sanity-check']:
        assert r.result == 'pass'
    for r in t.results['canterbury']:
        assert r.result in ('pass', 'skip')
    for r in t.results['didcot']:
        assert r.result == 'pass'
    for r in t.results['frome']:
        assert r.result == 'fail'
    for r in t.results['newport']:
        assert r.result == 'pass'
    for r in t.results['rhosydd']:
        assert r.result == 'pass'
    assert any(r.result == 'fail' for r in t.results['ribchester'])
    for r in t.results['ribchester']:
        assert r.result in ('pass', 'skip', 'fail')
    for r in t.results['traprain']:
        assert r.result == 'fail'

def test_register_and_update_1491564():
    registration = {
        'metadata': {
            'image.version': '20190215.0',
            'image.release': '19.03',
            'image.type': 'target',
            'image.arch': 'amd64',
            'image.board':'uefi',
            'image.osname': 'apertis',
            'image.flavour': '',
            'image.name': 'apertis_19.03-target-amd64-uefi_20190215.0.img.gz'
        },
        'status_string': 'submitted',
        'id': 1491564,
        'token': '865b4e70-667d-4a5f-8416-2dfd76a4ca21'
    }
    t_registered = TestJob(registration)
    with open_relative('./test_data/job_data_1491564') as f:
        rawdata = json.load(f)
    t_registered.update(rawdata)

    t_unregistered = TestJob(rawdata)

    assert t_registered.id == t_unregistered.id
    assert t_registered.metadata == t_unregistered.metadata
    assert t_registered.data == t_unregistered.data
    assert t_registered.results == t_unregistered.results

def test_self_update_1491564():
    with open_relative('./test_data/job_data_1491564') as f:
        rawdata = json.load(f)
    t_pristine = TestJob(rawdata)
    t_updated = TestJob(rawdata)
    t_updated.update(rawdata)

    assert t_pristine.id == t_updated.id
    assert t_pristine.description == t_updated.description
    assert t_pristine.metadata == t_updated.metadata
    assert t_pristine.data == t_updated.data
    assert t_pristine.results == t_updated.results

def test_load_1560987():
    with open_relative('./test_data/job_data_1560987') as f:
        rawdata = json.load(f)
    t = TestJob(rawdata)
    assert list(sorted(t.results.keys())) == [
            'apparmor',
            'apparmor-basic-profiles',
            'apparmor-chaiwala-system',
            'apparmor-dbus',
            'apparmor-functional-demo',
            'apparmor-geoclue',
            'apparmor-gstreamer1-0',
            'apparmor-pulseaudio',
            'apparmor-tracker',
            'apparmor-tumbler',
            'apparmor-utils',
            'disk-fsck-boot-rootfs',
            'sanity-check',
            ]

def test_load_1560988_incomplete():
    with open_relative('./test_data/job_data_1560988') as f:
        rawdata = json.load(f)
    t = TestJob(rawdata)
    assert list(sorted(t.results.keys())) == [
            'boot-no-crashes',
            'boot-performance',
            'cgroups-resource-control',
            'check-dbus-services',
            'connman',
            'connman-new-supplicant',
            'dbus-dos-reply-time',
            'dbus-installed-tests',
            'disk-fsck-boot-rootfs',
            'gettext-i18n',
            'glib-gio-fs',
            'grilo',
            'gstreamer1-0-decode',
            'gupnp-services',
            'iptables-basic',
            'libsoup-unit',
            'polkit-parsing',
            'sanity-check',
            'tracker-indexing-local-storage'
            ]
    dbus_installed_tests = list(t.results['dbus-installed-tests'])[0]
    assert dbus_installed_tests.result == 'incomplete'
    assert dbus_installed_tests.url == '/results/1560988'

def test_load_1577786():
    with open_relative('./test_data/job_data_1577786') as f:
        rawdata = json.load(f)
    t = TestJob(rawdata)

def test_load_1663435():
    with open_relative('./test_data/job_data_1663435') as f:
        rawdata = json.load(f)
    t = TestJob(rawdata)
    assert t.metadata['image.deployment'] == 'nfs'
    assert t.metadata['image.flavour'] == 'nfs'
    assert list(sorted(t.results.keys())) == [
            'nfsroot-simple-boot'
            ]

def test_load_2640299():
    with open_relative('./test_data/job_data_2640299') as f:
        rawdata = json.load(f)
    t = TestJob(rawdata)
    assert t.metadata['image.deployment'] == 'lxc'
    assert t.metadata['image.flavour'] == 'tiny'
    assert list(sorted(t.results.keys())) == [
            'tiny-container-system-aa-enforcement',
            'tiny-container-system-basic',
            'tiny-container-system-connectivity-profile',
            'tiny-container-system-device-sharing',
            'tiny-container-system-folder-sharing',
            'tiny-container-system-seccomp',
            'tiny-container-user-aa-enforcement',
            'tiny-container-user-basic',
            'tiny-container-user-connectivity-profile',
            'tiny-container-user-device-sharing',
            'tiny-container-user-folder-sharing',
            'tiny-container-user-seccomp',
            ]

def test_load_2406367():
    with open_relative('./test_data/job_data_2406367') as f:
        rawdata = json.load(f)
    t = TestJob(rawdata)
    aum_ota_signed = t.results['aum-ota-signed']
    assert len(aum_ota_signed) == 7
    assert all(r.testdefinition == 'aum-ota-signed' for r in aum_ota_signed)
    assert all(r.path == 'test-cases/aum-ota-signed.yaml' for r in aum_ota_signed)

def test_load_2775965():
    with open_relative('./test_data/job_data_2775965') as f:
        rawdata = json.load(f)
    t = TestJob(rawdata)
    assert t.status == 'incomplete'
    assert list(sorted(t.results.keys())) == [
            'add-repo',
            'boot-no-crashes',
            'boot-performance',
            'cgroups-resource-control',
            'connman',
            'connman-new-supplicant',
            'dbus-dos-reply-time',
            'disk-rootfs-fsck',
            'glib-gio-fs',
            'gupnp-services',
            'iptables-basic',
            'sanity-check',
            ]
    assert all(next(iter(r)).result == 'incomplete' for r in t.results.values())
