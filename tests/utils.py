import os

def open_relative(path, *args, **kwargs):
    """Resolve data files relative to this file"""
    dir_path = os.path.dirname(os.path.realpath(__file__))
    final_path = os.path.join(dir_path, path)
    return open(final_path, *args, **kwargs)
