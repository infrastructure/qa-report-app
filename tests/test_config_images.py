import sys

import pytest

# Import modules from the current and parent directory in case pytest is
# invoked in this folder or from the project root
sys.path += ['.', '..']

from config import ImagesConfig


def test_deployments():
    data = {
        'architectures': [],
        'variants': [],
        'platform-groups': [],
        'deployments': {
            'user-selectable': {
                'test': 'A Test',
                'test-2': 'Another Test',
            },
            'lava-only': {'lava-1', 'lava-2'},
        },
    }
    config = ImagesConfig.parse_obj(data)

    assert set(config.deployments.all_names) == {'test', 'test-2', 'lava-1', 'lava-2'}
    assert config.deployments.user_selectable == {'test': 'A Test', 'test-2': 'Another Test'}
    assert config.deployments.lava_only == {'lava-1', 'lava-2'}

    data['deployments']['lava-only'].add('test')
    with pytest.raises(ValueError, match='duplicate deployment'):
        ImagesConfig.parse_obj(data)


def test_platform_group_validation():
    platforms_1 = {
        'hmi-arm64-things': 'HMI ARM64 (things)',
        'tiny-lxc-amd64-more-things': 'Tiny LXC AMD64 (more things)',
    }

    platforms_2 = {
        'hmi-amd64-things': 'HMI AMD64 (things)',
        'tiny-lxc-arm64-more-things': 'Tiny LXC ARM64 (more things)',
    }

    data = {
        'architectures': ['arm64', 'amd64'],
        'variants': ['tiny-lxc', 'hmi'],
        'deployments': {
            'user-selectable': {
                'user-deployment-1': 'User Deployment 1',
                'user-deployment-2': 'User Deployment 2',
            },
            'lava-only': ['lava-deployment-1', 'lava-deployment-2'],
        },
        'platform-groups': [
            {
                'deployments': {'user-deployment-1', 'lava-deployment-2'},
                'platforms': platforms_1,
            },
            {
                'deployments': {'lava-deployment-1', 'user-deployment-2'},
                'platforms': platforms_2,
            }
        ],
    }

    config = ImagesConfig.parse_obj(data)
    assert len(config.platform_groups) == 2
    assert config.platform_groups[0].platforms == platforms_1
    assert config.platform_groups[1].platforms == platforms_2

    data['platform-groups'][0]['platforms'] = {
        'unknown-arm64-things': 'Unknown ARM64',
    }
    with pytest.raises(ValueError, match='unknown variant'):
        ImagesConfig.parse_obj(data)

    data['platform-groups'][0]['platforms'] = {
        'hmi-armhf-things': 'HMI ARMHF',
    }
    with pytest.raises(ValueError, match='unknown arch'):
        ImagesConfig.parse_obj(data)

    data['platform-groups'][0]['platforms'] = platforms_1

    data['platform-groups'][0]['deployments'] = {'unknown-deployment-1'}
    with pytest.raises(ValueError, match='unknown deployments'):
        ImagesConfig.parse_obj(data)


def test_support_range_constriants():
    constraint = ImagesConfig.PlatformGroup.SupportRange.Constraint.parse_obj({
        'release': 'v2022',
        'version': '2',
    })

    assert constraint.older_than(release='v2023', version=None)
    assert constraint.older_than(release='v2022', version='3')
    assert not constraint.older_than(release='v2022pre', version='3')
    assert not constraint.older_than(release='v2021', version='3')

    assert constraint.equals(release='v2022', version='2')
    assert constraint.equals(release='v2022', version=None)
    assert not constraint.equals(release='v2022', version='3')
    assert not constraint.equals(release='v2022pre', version='2')
    assert not constraint.equals(release='v2023', version='2')

    constraint = ImagesConfig.PlatformGroup.SupportRange.Constraint.parse_obj({
        'release': 'v2022',
    })

    assert constraint.older_than(release='v2023', version=None)
    assert not constraint.older_than(release='v2022', version='3')
    assert not constraint.older_than(release='v2022pre', version=None)
    assert not constraint.older_than(release='v2021', version=None)

    assert constraint.equals(release='v2022', version='2')
    assert constraint.equals(release='v2022', version=None)
    assert not constraint.equals(release='v2022pre', version=None)
    assert not constraint.equals(release='v2023', version=None)


def test_support_range_validation():
    with pytest.raises(ValueError, match='one of'):
        ImagesConfig.PlatformGroup.SupportRange.parse_obj({})

    with pytest.raises(ValueError, match='one of'):
        ImagesConfig.PlatformGroup.SupportRange.parse_obj({
            'first': None,
            'last': None,
        })

    with pytest.raises(ValueError, match='older than'):
        ImagesConfig.PlatformGroup.SupportRange.parse_obj({
            'first': {
                'release': 'v2022',
            },
            'last': {
                'release': 'v2021',
            }
        })

    with pytest.raises(ValueError, match='older than'):
        ImagesConfig.PlatformGroup.SupportRange.parse_obj({
            'first': {
                'release': 'v2022',
                'version': '2',
            },
            'last': {
                'release': 'v2022',
                'version': '1',
            }
        })

    with pytest.raises(ValueError, match='overlap'):
        ImagesConfig.PlatformGroup.SupportRange.parse_obj({
            'first': {
                'release': 'v2022',
            },
            'last': {
                'release': 'v2022',
            }
        })

    with pytest.raises(ValueError, match='overlap'):
        ImagesConfig.PlatformGroup.SupportRange.parse_obj({
            'first': {
                'release': 'v2022',
            },
            'last': {
                'release': 'v2022',
                'version': '2',
            }
        })


def test_support_range_includes():
    support_range = ImagesConfig.PlatformGroup.SupportRange.parse_obj({
        'first': {
            'release': 'v2022',
        },
    })

    assert support_range.includes(release='v2022', version=None)
    assert support_range.includes(release='v2022', version='2')
    assert support_range.includes(release='v2023', version='2')
    assert not support_range.includes(release='v2022pre', version=None)
    assert not support_range.includes(release='v2021', version='2')

    support_range = ImagesConfig.PlatformGroup.SupportRange.parse_obj({
        'first': {
            'release': 'v2022',
            'inclusive': False,
        },
    })

    assert not support_range.includes(release='v2022', version=None)
    assert not support_range.includes(release='v2022', version='2')
    assert support_range.includes(release='v2023', version='2')
    assert not support_range.includes(release='v2022pre', version=None)
    assert not support_range.includes(release='v2021', version='2')

    support_range = ImagesConfig.PlatformGroup.SupportRange.parse_obj({
        'first': {
            'release': 'v2022',
            'version': '2',
        },
    })

    assert support_range.includes(release='v2022', version=None)
    assert support_range.includes(release='v2022', version='2')
    assert support_range.includes(release='v2022', version='3')
    assert support_range.includes(release='v2023', version='1')
    assert not support_range.includes(release='v2022pre', version=None)
    assert not support_range.includes(release='v2021', version='2')

    support_range = ImagesConfig.PlatformGroup.SupportRange.parse_obj({
        'first': {
            'release': 'v2022',
            'version': '2',
            'inclusive': False,
        },
    })

    assert support_range.includes(release='v2022', version=None)
    assert not support_range.includes(release='v2022', version='2')
    assert support_range.includes(release='v2022', version='3')
    assert support_range.includes(release='v2023', version='1')
    assert not support_range.includes(release='v2022pre', version=None)
    assert not support_range.includes(release='v2021', version='2')

    support_range = ImagesConfig.PlatformGroup.SupportRange.parse_obj({
        'last': {
            'release': 'v2022',
        },
    })

    assert support_range.includes(release='v2022', version=None)
    assert support_range.includes(release='v2022', version='2')
    assert not support_range.includes(release='v2023', version='2')
    assert support_range.includes(release='v2022pre', version=None)
    assert support_range.includes(release='v2021', version='2')

    support_range = ImagesConfig.PlatformGroup.SupportRange.parse_obj({
        'last': {
            'release': 'v2022',
            'inclusive': False,
        },
    })

    assert not support_range.includes(release='v2022', version=None)
    assert not support_range.includes(release='v2022', version='2')
    assert not support_range.includes(release='v2023', version='2')
    assert support_range.includes(release='v2022pre', version=None)
    assert support_range.includes(release='v2021', version='2')

    support_range = ImagesConfig.PlatformGroup.SupportRange.parse_obj({
        'last': {
            'release': 'v2022',
            'version': '2',
        },
    })

    assert support_range.includes(release='v2022', version=None)
    assert support_range.includes(release='v2022', version='2')
    assert not support_range.includes(release='v2022', version='3')
    assert not support_range.includes(release='v2023', version='1')
    assert support_range.includes(release='v2022pre', version=None)
    assert support_range.includes(release='v2021', version='2')

    support_range = ImagesConfig.PlatformGroup.SupportRange.parse_obj({
        'last': {
            'release': 'v2022',
            'version': '2',
            'inclusive': False,
        },
    })

    assert support_range.includes(release='v2022', version=None)
    assert not support_range.includes(release='v2022', version='2')
    assert not support_range.includes(release='v2022', version='3')
    assert not support_range.includes(release='v2023', version='1')
    assert support_range.includes(release='v2022pre', version=None)
    assert support_range.includes(release='v2021', version='2')

    support_range = ImagesConfig.PlatformGroup.SupportRange.parse_obj({
        'first': {
            'release': 'v2021',
            'version': '3',
        },
        'last': {
            'release': 'v2023',
            'version': '2',
            'inclusive': False,
        },
    })

    assert not support_range.includes(release='v2020', version=None)
    assert not support_range.includes(release='v2021pre', version='4')
    assert support_range.includes(release='v2021', version=None)
    assert not support_range.includes(release='v2021', version='2')
    assert support_range.includes(release='v2021', version='3')
    assert support_range.includes(release='v2021', version='4')
    assert support_range.includes(release='v2022', version='1')
    assert support_range.includes(release='v2022', version=None)
    assert support_range.includes(release='v2023', version=None)
    assert support_range.includes(release='v2023', version='1')
    assert not support_range.includes(release='v2023', version='2')
    assert not support_range.includes(release='v2023', version='3')
    assert not support_range.includes(release='v2024', version=None)


def test_platform_group_matches():
    platform_group = ImagesConfig.PlatformGroup.parse_obj({
        'platforms': [],
        'deployments': [],
    })

    assert platform_group.matches(release='v2021', version=None)
    assert platform_group.matches(release='v2021', version='2')

    platform_group = ImagesConfig.PlatformGroup.parse_obj({
        'platforms': [],
        'deployments': [],
        'supported-by': [
            {
                'first': {
                    'release': 'v2021',
                    'version': '3',
                },
            },
            {
                'first': {
                    'release': 'v2019',
                },
                'last': {
                    'release': 'v2020',
                    'version': '2',
                },
            }
        ],
    })

    assert platform_group.matches(release='v2019', version=None)
    assert platform_group.matches(release='v2019', version='2')
    assert platform_group.matches(release='v2020pre', version='4')
    assert platform_group.matches(release='v2020', version='2')
    assert not platform_group.matches(release='v2020', version='3')
    assert platform_group.matches(release='v2021', version=None)
    assert not platform_group.matches(release='v2021', version='2')
    assert platform_group.matches(release='v2021', version='3')
    assert platform_group.matches(release='v2022', version=None)
    assert platform_group.matches(release='v2022', version='1')
