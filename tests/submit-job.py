#!/usr/bin/python3
###################################################################################
# LAVA CI callback webservice
#
# Job submission tool
#
# Copyright (C) 2018 Collabora Ltd
# Andrej Shadura <andrew.shadura@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import argparse
import json
import os
import sys
from urllib.request import Request, urlopen

prog = os.path.basename(sys.argv[0])

parser = argparse.ArgumentParser(description='post the job metadata to the LAVA callback webhook', prog=prog)
parser.add_argument('-u', '--url', type=str, required=True,
                    help="specify the callback URL")
parser.add_argument('--callback-secret', type=str,
                    help="specify the secret token to use for the callback")
parser.add_argument('job_id', metavar='JOB_ID', help='LAVA job id')
parser.add_argument('version', metavar='BUILD', help='image version (YYYYMMDD.X)')
parser.add_argument('release', metavar='RELEASE', help='release (YY.MM)')
parser.add_argument('arch', metavar='ARCH', help='architecture (e.g. amd64)')
parser.add_argument('board', metavar='BOARD', help='board (e.g. uefi)')
parser.add_argument('type', metavar='TYPE', help='image type (e.g. target)')

args = parser.parse_args()

token = None
headers = None

metadata = vars(args)
url = metadata['url']
del metadata['url']
job_id = metadata['job_id']
del metadata['job_id']
if 'callback_secret' in metadata:
    token = metadata['callback_secret']
    del metadata['callback_secret']

request = {
    'status_string': 'submitted',
    'metadata': {'image.' + k: v for k, v in metadata.items()},
    'id': int(job_id)
}
if token:
    headers = {'Authorization': token}

print(json.dumps(request))
req = Request(url, headers=headers, data=json.dumps(request).encode('UTF-8'))
urlopen(req)


