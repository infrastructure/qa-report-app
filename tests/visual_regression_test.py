# Copyright © 2019 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sys
import unittest

import flask_migrate
import flask_testing
import needle.cases
import pytest
import selenium.webdriver.chrome.options

from testing.postgresql import PostgresqlFactory

# Import modules from the current and parent directory in case pytest is
# invoked in this folder or from the project root
sys.path += ['.', '..']

import testutils
import config

testutils.needle_fixup_get_screenshot()
testutils.webdriver_fixup_disable_transitions()

def _populate_db(postgresql):
    """Load the saved jobs in test_data/ to populate the database so the pages
       have some actual contents in them"""
    app, db = testutils.import_app_with_custom_db_instance(postgresql)
    migrate = flask_migrate.Migrate(app, db)
    with app.app_context():
        flask_migrate.upgrade()
    job_ids = [
            1377334,
            1450933,
            1491564,
            1560987,
            1560988,
            1577786,
            1663435,
            2640299,
            4505732,
            ]
    for job_id in job_ids:
        testutils.submit_test_data(app, job_id)
    # Populate DB with manual results to test Notes section links
    testutils.submit_manual_test_data(app, db)

Postgresql = PostgresqlFactory(cache_initialized_db=True,
                             on_initialized=_populate_db)

def tearDownModule():
    Postgresql.clear_cache()

class VisualRegressionTestCase(flask_testing.LiveServerTestCase, needle.cases.NeedleTestCase):
    engine_class = 'needle.engines.perceptualdiff_engine.Engine'
    viewport_height=2048

    @classmethod
    def get_web_driver(cls):
        options = selenium.webdriver.chrome.options.Options()
        options.headless = True
        options.add_argument('--no-sandbox')
        return needle.cases.NeedleChrome(options=options)

    def create_app(self):
        self.postgresql = Postgresql()
        app, db = testutils.import_app_with_custom_db_instance(
            self.postgresql,
            config.Display.parse_obj(
                {'min-version': 'v2019',
                 'series-results':
                     {'dailies': 2, 'releases': 3, 'weeklies': 4}
                 })
        )

        # Load the test cases table in here, since
        # import_app_with_custom_db_instance reloads the entire webhook module.
        from webhook import testcases_table
        testcases_table.initialize()

        # Don't show newer releases than needed so that the tests don't have to
        # be updated every release.
        test_cases = testcases_table.get()
        for release in list(test_cases):
            if release > 'v2022dev3':
                test_cases.pop(release)

        return app

    def tearDown(self):
        self.postgresql.stop()

    def test_index(self):
        self.driver.get(self.get_server_url())
        self.assertScreenshot('html', 'index-page')

    def test_index_all(self):
        self.driver.get(self.get_server_url()+'/all.html')
        self.assertScreenshot('html', 'index-page-all')

    def test_image_rendering(self):
        # check the report and png image for the data in v2022/webkit2gtk-ac-3d-rendering.html
        test_case_path = '/testcases/v2022/webkit2gtk-ac-3d-rendering.html'
        self.driver.get(self.get_server_url()+test_case_path)
        self.assertScreenshot('html', 'webkit2gtk-ac-3d-rendering')

    def test_report_fixedfunction(self):
        # check the report for the data in test_data/job_data_4505732
        report_path = '/report/v2022dev3/20210913.0018/apt'
        self.driver.get(self.get_server_url()+report_path)
        self.assertScreenshot('html', 'report-v2022dev3-20210913.0018-apt')

    def test_report_lxc(self):
        # check the report for the data in test_data/job_data_2640299
        report_path = '/report/v2022dev0/20200920.2038/lxc'
        self.driver.get(self.get_server_url()+report_path)
        self.assertScreenshot('html', 'report-v2022dev0-20200920.2038-lxc')

    def test_report_notes(self):
        report_path = '/report/v2021dev1/20200213.0/ostree'
        self.driver.get(self.get_server_url()+report_path)
        # Scroll to the right most in order to check the Notes section.
        table = self.driver.find_element_by_tag_name('table')
        offset = "arguments[0].scrollLeft = arguments[0].offsetWidth"
        self.driver.execute_script(offset, table)
        self.assertScreenshot('html', 'report-v2021dev1-20200213.0-ostree')
