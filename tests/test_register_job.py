#!/usr/bin/env python3

import sys
import json
from urllib.error import URLError
from urllib.request import urlopen


if __name__ == '__main__':
    request = {
        'metadata': {
            'image.version': '20190215.0',
            'image.release': '19.03',
            'image.type': 'target',
            'image.arch': 'amd64',
            'image.board':'uefi',
            'image.osname': 'apertis',
            'image.flavour': '',
            'image.name': 'apertis_19.03-target-amd64-uefi_20190215.0.img.gz'
        },
        'status_string': 'submitted',
        'id': 1491564,
        'token': '865b4e70-667d-4a5f-8416-2dfd76a4ca21'
    }
    try:
        host = 'http://%s:28080/' % sys.argv[1]
        urlopen(host, data=json.dumps(request).encode('UTF-8'))
    except URLError:
        pass
